/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.controller;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import sv.srn.jpa.controller.exceptions.NonexistentEntityException;
import sv.srn.jpa.entity.Bodega;
import sv.srn.jpa.entity.BodegaExistencia;
import sv.srn.jpa.entity.Producto;

/**
 *
 * @author angel
 */
public class BodegaExistenciaJpaController implements Serializable {

    public BodegaExistenciaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(BodegaExistencia bodegaExistencia) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Bodega idBodegaBxp = bodegaExistencia.getIdBodegaBxp();
            if (idBodegaBxp != null) {
                idBodegaBxp = em.getReference(idBodegaBxp.getClass(), idBodegaBxp.getIdBodega());
                bodegaExistencia.setIdBodegaBxp(idBodegaBxp);
            }
            Producto idProductoBxp = bodegaExistencia.getIdProductoBxp();
            if (idProductoBxp != null) {
                idProductoBxp = em.getReference(idProductoBxp.getClass(), idProductoBxp.getIdProducto());
                bodegaExistencia.setIdProductoBxp(idProductoBxp);
            }
            em.persist(bodegaExistencia);
            if (idBodegaBxp != null) {
                idBodegaBxp.getBodegaExistenciaCollection().add(bodegaExistencia);
                idBodegaBxp = em.merge(idBodegaBxp);
            }
            if (idProductoBxp != null) {
                idProductoBxp.getBodegaExistenciaCollection().add(bodegaExistencia);
                idProductoBxp = em.merge(idProductoBxp);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(BodegaExistencia bodegaExistencia) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            BodegaExistencia persistentBodegaExistencia = em.find(BodegaExistencia.class, bodegaExistencia.getIdbodegaExistencia());
            Bodega idBodegaBxpOld = persistentBodegaExistencia.getIdBodegaBxp();
            Bodega idBodegaBxpNew = bodegaExistencia.getIdBodegaBxp();
            Producto idProductoBxpOld = persistentBodegaExistencia.getIdProductoBxp();
            Producto idProductoBxpNew = bodegaExistencia.getIdProductoBxp();
            if (idBodegaBxpNew != null) {
                idBodegaBxpNew = em.getReference(idBodegaBxpNew.getClass(), idBodegaBxpNew.getIdBodega());
                bodegaExistencia.setIdBodegaBxp(idBodegaBxpNew);
            }
            if (idProductoBxpNew != null) {
                idProductoBxpNew = em.getReference(idProductoBxpNew.getClass(), idProductoBxpNew.getIdProducto());
                bodegaExistencia.setIdProductoBxp(idProductoBxpNew);
            }
            bodegaExistencia = em.merge(bodegaExistencia);
            if (idBodegaBxpOld != null && !idBodegaBxpOld.equals(idBodegaBxpNew)) {
                idBodegaBxpOld.getBodegaExistenciaCollection().remove(bodegaExistencia);
                idBodegaBxpOld = em.merge(idBodegaBxpOld);
            }
            if (idBodegaBxpNew != null && !idBodegaBxpNew.equals(idBodegaBxpOld)) {
                idBodegaBxpNew.getBodegaExistenciaCollection().add(bodegaExistencia);
                idBodegaBxpNew = em.merge(idBodegaBxpNew);
            }
            if (idProductoBxpOld != null && !idProductoBxpOld.equals(idProductoBxpNew)) {
                idProductoBxpOld.getBodegaExistenciaCollection().remove(bodegaExistencia);
                idProductoBxpOld = em.merge(idProductoBxpOld);
            }
            if (idProductoBxpNew != null && !idProductoBxpNew.equals(idProductoBxpOld)) {
                idProductoBxpNew.getBodegaExistenciaCollection().add(bodegaExistencia);
                idProductoBxpNew = em.merge(idProductoBxpNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = bodegaExistencia.getIdbodegaExistencia();
                if (findBodegaExistencia(id) == null) {
                    throw new NonexistentEntityException("The bodegaExistencia with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            BodegaExistencia bodegaExistencia;
            try {
                bodegaExistencia = em.getReference(BodegaExistencia.class, id);
                bodegaExistencia.getIdbodegaExistencia();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The bodegaExistencia with id " + id + " no longer exists.", enfe);
            }
            Bodega idBodegaBxp = bodegaExistencia.getIdBodegaBxp();
            if (idBodegaBxp != null) {
                idBodegaBxp.getBodegaExistenciaCollection().remove(bodegaExistencia);
                idBodegaBxp = em.merge(idBodegaBxp);
            }
            Producto idProductoBxp = bodegaExistencia.getIdProductoBxp();
            if (idProductoBxp != null) {
                idProductoBxp.getBodegaExistenciaCollection().remove(bodegaExistencia);
                idProductoBxp = em.merge(idProductoBxp);
            }
            em.remove(bodegaExistencia);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<BodegaExistencia> findBodegaExistenciaEntities() {
        return findBodegaExistenciaEntities(true, -1, -1);
    }

    public List<BodegaExistencia> findBodegaExistenciaEntities(int maxResults, int firstResult) {
        return findBodegaExistenciaEntities(false, maxResults, firstResult);
    }

    private List<BodegaExistencia> findBodegaExistenciaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(BodegaExistencia.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public BodegaExistencia findBodegaExistencia(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(BodegaExistencia.class, id);
        } finally {
            em.close();
        }
    }

    public int getBodegaExistenciaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<BodegaExistencia> rt = cq.from(BodegaExistencia.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

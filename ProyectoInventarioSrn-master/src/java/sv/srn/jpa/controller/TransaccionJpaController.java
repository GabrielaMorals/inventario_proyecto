/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.controller;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import sv.srn.jpa.controller.exceptions.NonexistentEntityException;
import sv.srn.jpa.controller.exceptions.PreexistingEntityException;
import sv.srn.jpa.entity.Bodega;
import sv.srn.jpa.entity.Producto;
import sv.srn.jpa.entity.Proveedor;
import sv.srn.jpa.entity.TipoTransaccion;
import sv.srn.jpa.entity.Transaccion;
import sv.srn.jpa.entity.Usuarios;

/**
 *
 * @author angel
 */
public class TransaccionJpaController implements Serializable {

    public TransaccionJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Transaccion transaccion) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Bodega idBodega = transaccion.getIdBodega();
            if (idBodega != null) {
                idBodega = em.getReference(idBodega.getClass(), idBodega.getIdBodega());
                transaccion.setIdBodega(idBodega);
            }
            Producto idProducto = transaccion.getIdProducto();
            if (idProducto != null) {
                idProducto = em.getReference(idProducto.getClass(), idProducto.getIdProducto());
                transaccion.setIdProducto(idProducto);
            }
            Proveedor idProveedor = transaccion.getIdProveedor();
            if (idProveedor != null) {
                idProveedor = em.getReference(idProveedor.getClass(), idProveedor.getIdProveedor());
                transaccion.setIdProveedor(idProveedor);
            }
            TipoTransaccion idTipoTransaccion = transaccion.getIdTipoTransaccion();
            if (idTipoTransaccion != null) {
                idTipoTransaccion = em.getReference(idTipoTransaccion.getClass(), idTipoTransaccion.getIdTipoTran());
                transaccion.setIdTipoTransaccion(idTipoTransaccion);
            }
            Usuarios idUsuario = transaccion.getIdUsuario();
            if (idUsuario != null) {
                idUsuario = em.getReference(idUsuario.getClass(), idUsuario.getIdUsuario());
                transaccion.setIdUsuario(idUsuario);
            }
            em.persist(transaccion);
            if (idBodega != null) {
                idBodega.getTransaccionCollection().add(transaccion);
                idBodega = em.merge(idBodega);
            }
            if (idProducto != null) {
                idProducto.getTransaccionCollection().add(transaccion);
                idProducto = em.merge(idProducto);
            }
            if (idProveedor != null) {
                idProveedor.getTransaccionCollection().add(transaccion);
                idProveedor = em.merge(idProveedor);
            }
            if (idTipoTransaccion != null) {
                idTipoTransaccion.getTransaccionCollection().add(transaccion);
                idTipoTransaccion = em.merge(idTipoTransaccion);
            }
            if (idUsuario != null) {
                idUsuario.getTransaccionCollection().add(transaccion);
                idUsuario = em.merge(idUsuario);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTransaccion(transaccion.getIdTransaccion()) != null) {
                throw new PreexistingEntityException("Transaccion " + transaccion + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Transaccion transaccion) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Transaccion persistentTransaccion = em.find(Transaccion.class, transaccion.getIdTransaccion());
            Bodega idBodegaOld = persistentTransaccion.getIdBodega();
            Bodega idBodegaNew = transaccion.getIdBodega();
            Producto idProductoOld = persistentTransaccion.getIdProducto();
            Producto idProductoNew = transaccion.getIdProducto();
            Proveedor idProveedorOld = persistentTransaccion.getIdProveedor();
            Proveedor idProveedorNew = transaccion.getIdProveedor();
            TipoTransaccion idTipoTransaccionOld = persistentTransaccion.getIdTipoTransaccion();
            TipoTransaccion idTipoTransaccionNew = transaccion.getIdTipoTransaccion();
            Usuarios idUsuarioOld = persistentTransaccion.getIdUsuario();
            Usuarios idUsuarioNew = transaccion.getIdUsuario();
            if (idBodegaNew != null) {
                idBodegaNew = em.getReference(idBodegaNew.getClass(), idBodegaNew.getIdBodega());
                transaccion.setIdBodega(idBodegaNew);
            }
            if (idProductoNew != null) {
                idProductoNew = em.getReference(idProductoNew.getClass(), idProductoNew.getIdProducto());
                transaccion.setIdProducto(idProductoNew);
            }
            if (idProveedorNew != null) {
                idProveedorNew = em.getReference(idProveedorNew.getClass(), idProveedorNew.getIdProveedor());
                transaccion.setIdProveedor(idProveedorNew);
            }
            if (idTipoTransaccionNew != null) {
                idTipoTransaccionNew = em.getReference(idTipoTransaccionNew.getClass(), idTipoTransaccionNew.getIdTipoTran());
                transaccion.setIdTipoTransaccion(idTipoTransaccionNew);
            }
            if (idUsuarioNew != null) {
                idUsuarioNew = em.getReference(idUsuarioNew.getClass(), idUsuarioNew.getIdUsuario());
                transaccion.setIdUsuario(idUsuarioNew);
            }
            transaccion = em.merge(transaccion);
            if (idBodegaOld != null && !idBodegaOld.equals(idBodegaNew)) {
                idBodegaOld.getTransaccionCollection().remove(transaccion);
                idBodegaOld = em.merge(idBodegaOld);
            }
            if (idBodegaNew != null && !idBodegaNew.equals(idBodegaOld)) {
                idBodegaNew.getTransaccionCollection().add(transaccion);
                idBodegaNew = em.merge(idBodegaNew);
            }
            if (idProductoOld != null && !idProductoOld.equals(idProductoNew)) {
                idProductoOld.getTransaccionCollection().remove(transaccion);
                idProductoOld = em.merge(idProductoOld);
            }
            if (idProductoNew != null && !idProductoNew.equals(idProductoOld)) {
                idProductoNew.getTransaccionCollection().add(transaccion);
                idProductoNew = em.merge(idProductoNew);
            }
            if (idProveedorOld != null && !idProveedorOld.equals(idProveedorNew)) {
                idProveedorOld.getTransaccionCollection().remove(transaccion);
                idProveedorOld = em.merge(idProveedorOld);
            }
            if (idProveedorNew != null && !idProveedorNew.equals(idProveedorOld)) {
                idProveedorNew.getTransaccionCollection().add(transaccion);
                idProveedorNew = em.merge(idProveedorNew);
            }
            if (idTipoTransaccionOld != null && !idTipoTransaccionOld.equals(idTipoTransaccionNew)) {
                idTipoTransaccionOld.getTransaccionCollection().remove(transaccion);
                idTipoTransaccionOld = em.merge(idTipoTransaccionOld);
            }
            if (idTipoTransaccionNew != null && !idTipoTransaccionNew.equals(idTipoTransaccionOld)) {
                idTipoTransaccionNew.getTransaccionCollection().add(transaccion);
                idTipoTransaccionNew = em.merge(idTipoTransaccionNew);
            }
            if (idUsuarioOld != null && !idUsuarioOld.equals(idUsuarioNew)) {
                idUsuarioOld.getTransaccionCollection().remove(transaccion);
                idUsuarioOld = em.merge(idUsuarioOld);
            }
            if (idUsuarioNew != null && !idUsuarioNew.equals(idUsuarioOld)) {
                idUsuarioNew.getTransaccionCollection().add(transaccion);
                idUsuarioNew = em.merge(idUsuarioNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = transaccion.getIdTransaccion();
                if (findTransaccion(id) == null) {
                    throw new NonexistentEntityException("The transaccion with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Transaccion transaccion;
            try {
                transaccion = em.getReference(Transaccion.class, id);
                transaccion.getIdTransaccion();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The transaccion with id " + id + " no longer exists.", enfe);
            }
            Bodega idBodega = transaccion.getIdBodega();
            if (idBodega != null) {
                idBodega.getTransaccionCollection().remove(transaccion);
                idBodega = em.merge(idBodega);
            }
            Producto idProducto = transaccion.getIdProducto();
            if (idProducto != null) {
                idProducto.getTransaccionCollection().remove(transaccion);
                idProducto = em.merge(idProducto);
            }
            Proveedor idProveedor = transaccion.getIdProveedor();
            if (idProveedor != null) {
                idProveedor.getTransaccionCollection().remove(transaccion);
                idProveedor = em.merge(idProveedor);
            }
            TipoTransaccion idTipoTransaccion = transaccion.getIdTipoTransaccion();
            if (idTipoTransaccion != null) {
                idTipoTransaccion.getTransaccionCollection().remove(transaccion);
                idTipoTransaccion = em.merge(idTipoTransaccion);
            }
            Usuarios idUsuario = transaccion.getIdUsuario();
            if (idUsuario != null) {
                idUsuario.getTransaccionCollection().remove(transaccion);
                idUsuario = em.merge(idUsuario);
            }
            em.remove(transaccion);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Transaccion> findTransaccionEntities() {
        return findTransaccionEntities(true, -1, -1);
    }

    public List<Transaccion> findTransaccionEntities(int maxResults, int firstResult) {
        return findTransaccionEntities(false, maxResults, firstResult);
    }

    private List<Transaccion> findTransaccionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Transaccion.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Transaccion findTransaccion(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Transaccion.class, id);
        } finally {
            em.close();
        }
    }

    public int getTransaccionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Transaccion> rt = cq.from(Transaccion.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

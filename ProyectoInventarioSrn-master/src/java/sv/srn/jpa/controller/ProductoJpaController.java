/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import sv.srn.jpa.entity.Categoria;
import sv.srn.jpa.entity.Medidas;
import sv.srn.jpa.entity.Transaccion;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import sv.srn.jpa.controller.exceptions.IllegalOrphanException;
import sv.srn.jpa.controller.exceptions.NonexistentEntityException;
import sv.srn.jpa.entity.BodegaExistencia;
import sv.srn.jpa.entity.Producto;

/**
 *
 * @author angel
 */
public class ProductoJpaController implements Serializable {

    public ProductoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Producto producto) {
        if (producto.getTransaccionCollection() == null) {
            producto.setTransaccionCollection(new ArrayList<Transaccion>());
        }
        if (producto.getBodegaExistenciaCollection() == null) {
            producto.setBodegaExistenciaCollection(new ArrayList<BodegaExistencia>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Categoria idCategoria = producto.getIdCategoria();
            if (idCategoria != null) {
                idCategoria = em.getReference(idCategoria.getClass(), idCategoria.getIdCategoria());
                producto.setIdCategoria(idCategoria);
            }
            Medidas idMedida = producto.getIdMedida();
            if (idMedida != null) {
                idMedida = em.getReference(idMedida.getClass(), idMedida.getIdMedida());
                producto.setIdMedida(idMedida);
            }
            Collection<Transaccion> attachedTransaccionCollection = new ArrayList<Transaccion>();
            for (Transaccion transaccionCollectionTransaccionToAttach : producto.getTransaccionCollection()) {
                transaccionCollectionTransaccionToAttach = em.getReference(transaccionCollectionTransaccionToAttach.getClass(), transaccionCollectionTransaccionToAttach.getIdTransaccion());
                attachedTransaccionCollection.add(transaccionCollectionTransaccionToAttach);
            }
            producto.setTransaccionCollection(attachedTransaccionCollection);
            Collection<BodegaExistencia> attachedBodegaExistenciaCollection = new ArrayList<BodegaExistencia>();
            for (BodegaExistencia bodegaExistenciaCollectionBodegaExistenciaToAttach : producto.getBodegaExistenciaCollection()) {
                bodegaExistenciaCollectionBodegaExistenciaToAttach = em.getReference(bodegaExistenciaCollectionBodegaExistenciaToAttach.getClass(), bodegaExistenciaCollectionBodegaExistenciaToAttach.getIdbodegaExistencia());
                attachedBodegaExistenciaCollection.add(bodegaExistenciaCollectionBodegaExistenciaToAttach);
            }
            producto.setBodegaExistenciaCollection(attachedBodegaExistenciaCollection);
            em.persist(producto);
            if (idCategoria != null) {
                idCategoria.getProductoCollection().add(producto);
                idCategoria = em.merge(idCategoria);
            }
            if (idMedida != null) {
                idMedida.getProductoCollection().add(producto);
                idMedida = em.merge(idMedida);
            }
            for (Transaccion transaccionCollectionTransaccion : producto.getTransaccionCollection()) {
                Producto oldIdProductoOfTransaccionCollectionTransaccion = transaccionCollectionTransaccion.getIdProducto();
                transaccionCollectionTransaccion.setIdProducto(producto);
                transaccionCollectionTransaccion = em.merge(transaccionCollectionTransaccion);
                if (oldIdProductoOfTransaccionCollectionTransaccion != null) {
                    oldIdProductoOfTransaccionCollectionTransaccion.getTransaccionCollection().remove(transaccionCollectionTransaccion);
                    oldIdProductoOfTransaccionCollectionTransaccion = em.merge(oldIdProductoOfTransaccionCollectionTransaccion);
                }
            }
            for (BodegaExistencia bodegaExistenciaCollectionBodegaExistencia : producto.getBodegaExistenciaCollection()) {
                Producto oldIdProductoBxpOfBodegaExistenciaCollectionBodegaExistencia = bodegaExistenciaCollectionBodegaExistencia.getIdProductoBxp();
                bodegaExistenciaCollectionBodegaExistencia.setIdProductoBxp(producto);
                bodegaExistenciaCollectionBodegaExistencia = em.merge(bodegaExistenciaCollectionBodegaExistencia);
                if (oldIdProductoBxpOfBodegaExistenciaCollectionBodegaExistencia != null) {
                    oldIdProductoBxpOfBodegaExistenciaCollectionBodegaExistencia.getBodegaExistenciaCollection().remove(bodegaExistenciaCollectionBodegaExistencia);
                    oldIdProductoBxpOfBodegaExistenciaCollectionBodegaExistencia = em.merge(oldIdProductoBxpOfBodegaExistenciaCollectionBodegaExistencia);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Producto producto) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Producto persistentProducto = em.find(Producto.class, producto.getIdProducto());
            Categoria idCategoriaOld = persistentProducto.getIdCategoria();
            Categoria idCategoriaNew = producto.getIdCategoria();
            Medidas idMedidaOld = persistentProducto.getIdMedida();
            Medidas idMedidaNew = producto.getIdMedida();
            Collection<Transaccion> transaccionCollectionOld = persistentProducto.getTransaccionCollection();
            Collection<Transaccion> transaccionCollectionNew = producto.getTransaccionCollection();
            Collection<BodegaExistencia> bodegaExistenciaCollectionOld = persistentProducto.getBodegaExistenciaCollection();
            Collection<BodegaExistencia> bodegaExistenciaCollectionNew = producto.getBodegaExistenciaCollection();
            List<String> illegalOrphanMessages = null;
            for (Transaccion transaccionCollectionOldTransaccion : transaccionCollectionOld) {
                if (!transaccionCollectionNew.contains(transaccionCollectionOldTransaccion)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Transaccion " + transaccionCollectionOldTransaccion + " since its idProducto field is not nullable.");
                }
            }
            for (BodegaExistencia bodegaExistenciaCollectionOldBodegaExistencia : bodegaExistenciaCollectionOld) {
                if (!bodegaExistenciaCollectionNew.contains(bodegaExistenciaCollectionOldBodegaExistencia)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain BodegaExistencia " + bodegaExistenciaCollectionOldBodegaExistencia + " since its idProductoBxp field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idCategoriaNew != null) {
                idCategoriaNew = em.getReference(idCategoriaNew.getClass(), idCategoriaNew.getIdCategoria());
                producto.setIdCategoria(idCategoriaNew);
            }
            if (idMedidaNew != null) {
                idMedidaNew = em.getReference(idMedidaNew.getClass(), idMedidaNew.getIdMedida());
                producto.setIdMedida(idMedidaNew);
            }
            Collection<Transaccion> attachedTransaccionCollectionNew = new ArrayList<Transaccion>();
            for (Transaccion transaccionCollectionNewTransaccionToAttach : transaccionCollectionNew) {
                transaccionCollectionNewTransaccionToAttach = em.getReference(transaccionCollectionNewTransaccionToAttach.getClass(), transaccionCollectionNewTransaccionToAttach.getIdTransaccion());
                attachedTransaccionCollectionNew.add(transaccionCollectionNewTransaccionToAttach);
            }
            transaccionCollectionNew = attachedTransaccionCollectionNew;
            producto.setTransaccionCollection(transaccionCollectionNew);
            Collection<BodegaExistencia> attachedBodegaExistenciaCollectionNew = new ArrayList<BodegaExistencia>();
            for (BodegaExistencia bodegaExistenciaCollectionNewBodegaExistenciaToAttach : bodegaExistenciaCollectionNew) {
                bodegaExistenciaCollectionNewBodegaExistenciaToAttach = em.getReference(bodegaExistenciaCollectionNewBodegaExistenciaToAttach.getClass(), bodegaExistenciaCollectionNewBodegaExistenciaToAttach.getIdbodegaExistencia());
                attachedBodegaExistenciaCollectionNew.add(bodegaExistenciaCollectionNewBodegaExistenciaToAttach);
            }
            bodegaExistenciaCollectionNew = attachedBodegaExistenciaCollectionNew;
            producto.setBodegaExistenciaCollection(bodegaExistenciaCollectionNew);
            producto = em.merge(producto);
            if (idCategoriaOld != null && !idCategoriaOld.equals(idCategoriaNew)) {
                idCategoriaOld.getProductoCollection().remove(producto);
                idCategoriaOld = em.merge(idCategoriaOld);
            }
            if (idCategoriaNew != null && !idCategoriaNew.equals(idCategoriaOld)) {
                idCategoriaNew.getProductoCollection().add(producto);
                idCategoriaNew = em.merge(idCategoriaNew);
            }
            if (idMedidaOld != null && !idMedidaOld.equals(idMedidaNew)) {
                idMedidaOld.getProductoCollection().remove(producto);
                idMedidaOld = em.merge(idMedidaOld);
            }
            if (idMedidaNew != null && !idMedidaNew.equals(idMedidaOld)) {
                idMedidaNew.getProductoCollection().add(producto);
                idMedidaNew = em.merge(idMedidaNew);
            }
            for (Transaccion transaccionCollectionNewTransaccion : transaccionCollectionNew) {
                if (!transaccionCollectionOld.contains(transaccionCollectionNewTransaccion)) {
                    Producto oldIdProductoOfTransaccionCollectionNewTransaccion = transaccionCollectionNewTransaccion.getIdProducto();
                    transaccionCollectionNewTransaccion.setIdProducto(producto);
                    transaccionCollectionNewTransaccion = em.merge(transaccionCollectionNewTransaccion);
                    if (oldIdProductoOfTransaccionCollectionNewTransaccion != null && !oldIdProductoOfTransaccionCollectionNewTransaccion.equals(producto)) {
                        oldIdProductoOfTransaccionCollectionNewTransaccion.getTransaccionCollection().remove(transaccionCollectionNewTransaccion);
                        oldIdProductoOfTransaccionCollectionNewTransaccion = em.merge(oldIdProductoOfTransaccionCollectionNewTransaccion);
                    }
                }
            }
            for (BodegaExistencia bodegaExistenciaCollectionNewBodegaExistencia : bodegaExistenciaCollectionNew) {
                if (!bodegaExistenciaCollectionOld.contains(bodegaExistenciaCollectionNewBodegaExistencia)) {
                    Producto oldIdProductoBxpOfBodegaExistenciaCollectionNewBodegaExistencia = bodegaExistenciaCollectionNewBodegaExistencia.getIdProductoBxp();
                    bodegaExistenciaCollectionNewBodegaExistencia.setIdProductoBxp(producto);
                    bodegaExistenciaCollectionNewBodegaExistencia = em.merge(bodegaExistenciaCollectionNewBodegaExistencia);
                    if (oldIdProductoBxpOfBodegaExistenciaCollectionNewBodegaExistencia != null && !oldIdProductoBxpOfBodegaExistenciaCollectionNewBodegaExistencia.equals(producto)) {
                        oldIdProductoBxpOfBodegaExistenciaCollectionNewBodegaExistencia.getBodegaExistenciaCollection().remove(bodegaExistenciaCollectionNewBodegaExistencia);
                        oldIdProductoBxpOfBodegaExistenciaCollectionNewBodegaExistencia = em.merge(oldIdProductoBxpOfBodegaExistenciaCollectionNewBodegaExistencia);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = producto.getIdProducto();
                if (findProducto(id) == null) {
                    throw new NonexistentEntityException("The producto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Producto producto;
            try {
                producto = em.getReference(Producto.class, id);
                producto.getIdProducto();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The producto with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Transaccion> transaccionCollectionOrphanCheck = producto.getTransaccionCollection();
            for (Transaccion transaccionCollectionOrphanCheckTransaccion : transaccionCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Producto (" + producto + ") cannot be destroyed since the Transaccion " + transaccionCollectionOrphanCheckTransaccion + " in its transaccionCollection field has a non-nullable idProducto field.");
            }
            Collection<BodegaExistencia> bodegaExistenciaCollectionOrphanCheck = producto.getBodegaExistenciaCollection();
            for (BodegaExistencia bodegaExistenciaCollectionOrphanCheckBodegaExistencia : bodegaExistenciaCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Producto (" + producto + ") cannot be destroyed since the BodegaExistencia " + bodegaExistenciaCollectionOrphanCheckBodegaExistencia + " in its bodegaExistenciaCollection field has a non-nullable idProductoBxp field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Categoria idCategoria = producto.getIdCategoria();
            if (idCategoria != null) {
                idCategoria.getProductoCollection().remove(producto);
                idCategoria = em.merge(idCategoria);
            }
            Medidas idMedida = producto.getIdMedida();
            if (idMedida != null) {
                idMedida.getProductoCollection().remove(producto);
                idMedida = em.merge(idMedida);
            }
            em.remove(producto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Producto> findProductoEntities() {
        return findProductoEntities(true, -1, -1);
    }

    public List<Producto> findProductoEntities(int maxResults, int firstResult) {
        return findProductoEntities(false, maxResults, firstResult);
    }

    private List<Producto> findProductoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Producto.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Producto findProducto(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Producto.class, id);
        } finally {
            em.close();
        }
    }

    public int getProductoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Producto> rt = cq.from(Producto.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import sv.srn.jpa.entity.Transaccion;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import sv.srn.jpa.controller.exceptions.IllegalOrphanException;
import sv.srn.jpa.controller.exceptions.NonexistentEntityException;
import sv.srn.jpa.entity.Bodega;
import sv.srn.jpa.entity.BodegaExistencia;

/**
 *
 * @author angel
 */
public class BodegaJpaController implements Serializable {

    public BodegaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Bodega bodega) {
        if (bodega.getTransaccionCollection() == null) {
            bodega.setTransaccionCollection(new ArrayList<Transaccion>());
        }
        if (bodega.getBodegaExistenciaCollection() == null) {
            bodega.setBodegaExistenciaCollection(new ArrayList<BodegaExistencia>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Transaccion> attachedTransaccionCollection = new ArrayList<Transaccion>();
            for (Transaccion transaccionCollectionTransaccionToAttach : bodega.getTransaccionCollection()) {
                transaccionCollectionTransaccionToAttach = em.getReference(transaccionCollectionTransaccionToAttach.getClass(), transaccionCollectionTransaccionToAttach.getIdTransaccion());
                attachedTransaccionCollection.add(transaccionCollectionTransaccionToAttach);
            }
            bodega.setTransaccionCollection(attachedTransaccionCollection);
            Collection<BodegaExistencia> attachedBodegaExistenciaCollection = new ArrayList<BodegaExistencia>();
            for (BodegaExistencia bodegaExistenciaCollectionBodegaExistenciaToAttach : bodega.getBodegaExistenciaCollection()) {
                bodegaExistenciaCollectionBodegaExistenciaToAttach = em.getReference(bodegaExistenciaCollectionBodegaExistenciaToAttach.getClass(), bodegaExistenciaCollectionBodegaExistenciaToAttach.getIdbodegaExistencia());
                attachedBodegaExistenciaCollection.add(bodegaExistenciaCollectionBodegaExistenciaToAttach);
            }
            bodega.setBodegaExistenciaCollection(attachedBodegaExistenciaCollection);
            em.persist(bodega);
            for (Transaccion transaccionCollectionTransaccion : bodega.getTransaccionCollection()) {
                Bodega oldIdBodegaOfTransaccionCollectionTransaccion = transaccionCollectionTransaccion.getIdBodega();
                transaccionCollectionTransaccion.setIdBodega(bodega);
                transaccionCollectionTransaccion = em.merge(transaccionCollectionTransaccion);
                if (oldIdBodegaOfTransaccionCollectionTransaccion != null) {
                    oldIdBodegaOfTransaccionCollectionTransaccion.getTransaccionCollection().remove(transaccionCollectionTransaccion);
                    oldIdBodegaOfTransaccionCollectionTransaccion = em.merge(oldIdBodegaOfTransaccionCollectionTransaccion);
                }
            }
            for (BodegaExistencia bodegaExistenciaCollectionBodegaExistencia : bodega.getBodegaExistenciaCollection()) {
                Bodega oldIdBodegaBxpOfBodegaExistenciaCollectionBodegaExistencia = bodegaExistenciaCollectionBodegaExistencia.getIdBodegaBxp();
                bodegaExistenciaCollectionBodegaExistencia.setIdBodegaBxp(bodega);
                bodegaExistenciaCollectionBodegaExistencia = em.merge(bodegaExistenciaCollectionBodegaExistencia);
                if (oldIdBodegaBxpOfBodegaExistenciaCollectionBodegaExistencia != null) {
                    oldIdBodegaBxpOfBodegaExistenciaCollectionBodegaExistencia.getBodegaExistenciaCollection().remove(bodegaExistenciaCollectionBodegaExistencia);
                    oldIdBodegaBxpOfBodegaExistenciaCollectionBodegaExistencia = em.merge(oldIdBodegaBxpOfBodegaExistenciaCollectionBodegaExistencia);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Bodega bodega) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Bodega persistentBodega = em.find(Bodega.class, bodega.getIdBodega());
            Collection<Transaccion> transaccionCollectionOld = persistentBodega.getTransaccionCollection();
            Collection<Transaccion> transaccionCollectionNew = bodega.getTransaccionCollection();
            Collection<BodegaExistencia> bodegaExistenciaCollectionOld = persistentBodega.getBodegaExistenciaCollection();
            Collection<BodegaExistencia> bodegaExistenciaCollectionNew = bodega.getBodegaExistenciaCollection();
            List<String> illegalOrphanMessages = null;
            for (Transaccion transaccionCollectionOldTransaccion : transaccionCollectionOld) {
                if (!transaccionCollectionNew.contains(transaccionCollectionOldTransaccion)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Transaccion " + transaccionCollectionOldTransaccion + " since its idBodega field is not nullable.");
                }
            }
            for (BodegaExistencia bodegaExistenciaCollectionOldBodegaExistencia : bodegaExistenciaCollectionOld) {
                if (!bodegaExistenciaCollectionNew.contains(bodegaExistenciaCollectionOldBodegaExistencia)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain BodegaExistencia " + bodegaExistenciaCollectionOldBodegaExistencia + " since its idBodegaBxp field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Transaccion> attachedTransaccionCollectionNew = new ArrayList<Transaccion>();
            for (Transaccion transaccionCollectionNewTransaccionToAttach : transaccionCollectionNew) {
                transaccionCollectionNewTransaccionToAttach = em.getReference(transaccionCollectionNewTransaccionToAttach.getClass(), transaccionCollectionNewTransaccionToAttach.getIdTransaccion());
                attachedTransaccionCollectionNew.add(transaccionCollectionNewTransaccionToAttach);
            }
            transaccionCollectionNew = attachedTransaccionCollectionNew;
            bodega.setTransaccionCollection(transaccionCollectionNew);
            Collection<BodegaExistencia> attachedBodegaExistenciaCollectionNew = new ArrayList<BodegaExistencia>();
            for (BodegaExistencia bodegaExistenciaCollectionNewBodegaExistenciaToAttach : bodegaExistenciaCollectionNew) {
                bodegaExistenciaCollectionNewBodegaExistenciaToAttach = em.getReference(bodegaExistenciaCollectionNewBodegaExistenciaToAttach.getClass(), bodegaExistenciaCollectionNewBodegaExistenciaToAttach.getIdbodegaExistencia());
                attachedBodegaExistenciaCollectionNew.add(bodegaExistenciaCollectionNewBodegaExistenciaToAttach);
            }
            bodegaExistenciaCollectionNew = attachedBodegaExistenciaCollectionNew;
            bodega.setBodegaExistenciaCollection(bodegaExistenciaCollectionNew);
            bodega = em.merge(bodega);
            for (Transaccion transaccionCollectionNewTransaccion : transaccionCollectionNew) {
                if (!transaccionCollectionOld.contains(transaccionCollectionNewTransaccion)) {
                    Bodega oldIdBodegaOfTransaccionCollectionNewTransaccion = transaccionCollectionNewTransaccion.getIdBodega();
                    transaccionCollectionNewTransaccion.setIdBodega(bodega);
                    transaccionCollectionNewTransaccion = em.merge(transaccionCollectionNewTransaccion);
                    if (oldIdBodegaOfTransaccionCollectionNewTransaccion != null && !oldIdBodegaOfTransaccionCollectionNewTransaccion.equals(bodega)) {
                        oldIdBodegaOfTransaccionCollectionNewTransaccion.getTransaccionCollection().remove(transaccionCollectionNewTransaccion);
                        oldIdBodegaOfTransaccionCollectionNewTransaccion = em.merge(oldIdBodegaOfTransaccionCollectionNewTransaccion);
                    }
                }
            }
            for (BodegaExistencia bodegaExistenciaCollectionNewBodegaExistencia : bodegaExistenciaCollectionNew) {
                if (!bodegaExistenciaCollectionOld.contains(bodegaExistenciaCollectionNewBodegaExistencia)) {
                    Bodega oldIdBodegaBxpOfBodegaExistenciaCollectionNewBodegaExistencia = bodegaExistenciaCollectionNewBodegaExistencia.getIdBodegaBxp();
                    bodegaExistenciaCollectionNewBodegaExistencia.setIdBodegaBxp(bodega);
                    bodegaExistenciaCollectionNewBodegaExistencia = em.merge(bodegaExistenciaCollectionNewBodegaExistencia);
                    if (oldIdBodegaBxpOfBodegaExistenciaCollectionNewBodegaExistencia != null && !oldIdBodegaBxpOfBodegaExistenciaCollectionNewBodegaExistencia.equals(bodega)) {
                        oldIdBodegaBxpOfBodegaExistenciaCollectionNewBodegaExistencia.getBodegaExistenciaCollection().remove(bodegaExistenciaCollectionNewBodegaExistencia);
                        oldIdBodegaBxpOfBodegaExistenciaCollectionNewBodegaExistencia = em.merge(oldIdBodegaBxpOfBodegaExistenciaCollectionNewBodegaExistencia);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = bodega.getIdBodega();
                if (findBodega(id) == null) {
                    throw new NonexistentEntityException("The bodega with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Bodega bodega;
            try {
                bodega = em.getReference(Bodega.class, id);
                bodega.getIdBodega();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The bodega with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Transaccion> transaccionCollectionOrphanCheck = bodega.getTransaccionCollection();
            for (Transaccion transaccionCollectionOrphanCheckTransaccion : transaccionCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Bodega (" + bodega + ") cannot be destroyed since the Transaccion " + transaccionCollectionOrphanCheckTransaccion + " in its transaccionCollection field has a non-nullable idBodega field.");
            }
            Collection<BodegaExistencia> bodegaExistenciaCollectionOrphanCheck = bodega.getBodegaExistenciaCollection();
            for (BodegaExistencia bodegaExistenciaCollectionOrphanCheckBodegaExistencia : bodegaExistenciaCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Bodega (" + bodega + ") cannot be destroyed since the BodegaExistencia " + bodegaExistenciaCollectionOrphanCheckBodegaExistencia + " in its bodegaExistenciaCollection field has a non-nullable idBodegaBxp field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(bodega);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Bodega> findBodegaEntities() {
        return findBodegaEntities(true, -1, -1);
    }

    public List<Bodega> findBodegaEntities(int maxResults, int firstResult) {
        return findBodegaEntities(false, maxResults, firstResult);
    }

    private List<Bodega> findBodegaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Bodega.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Bodega findBodega(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Bodega.class, id);
        } finally {
            em.close();
        }
    }

    public int getBodegaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Bodega> rt = cq.from(Bodega.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

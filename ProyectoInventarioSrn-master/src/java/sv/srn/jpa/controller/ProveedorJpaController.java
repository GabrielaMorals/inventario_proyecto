/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import sv.srn.jpa.entity.Municipio;
import sv.srn.jpa.entity.Transaccion;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import sv.srn.jpa.controller.exceptions.IllegalOrphanException;
import sv.srn.jpa.controller.exceptions.NonexistentEntityException;
import sv.srn.jpa.entity.Proveedor;

/**
 *
 * @author angel
 */
public class ProveedorJpaController implements Serializable {

    public ProveedorJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Proveedor proveedor) {
        if (proveedor.getTransaccionCollection() == null) {
            proveedor.setTransaccionCollection(new ArrayList<Transaccion>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Municipio idMunicipioPrv = proveedor.getIdMunicipioPrv();
            if (idMunicipioPrv != null) {
                idMunicipioPrv = em.getReference(idMunicipioPrv.getClass(), idMunicipioPrv.getIdMunicipio());
                proveedor.setIdMunicipioPrv(idMunicipioPrv);
            }
            Collection<Transaccion> attachedTransaccionCollection = new ArrayList<Transaccion>();
            for (Transaccion transaccionCollectionTransaccionToAttach : proveedor.getTransaccionCollection()) {
                transaccionCollectionTransaccionToAttach = em.getReference(transaccionCollectionTransaccionToAttach.getClass(), transaccionCollectionTransaccionToAttach.getIdTransaccion());
                attachedTransaccionCollection.add(transaccionCollectionTransaccionToAttach);
            }
            proveedor.setTransaccionCollection(attachedTransaccionCollection);
            em.persist(proveedor);
            if (idMunicipioPrv != null) {
                idMunicipioPrv.getProveedorCollection().add(proveedor);
                idMunicipioPrv = em.merge(idMunicipioPrv);
            }
            for (Transaccion transaccionCollectionTransaccion : proveedor.getTransaccionCollection()) {
                Proveedor oldIdProveedorOfTransaccionCollectionTransaccion = transaccionCollectionTransaccion.getIdProveedor();
                transaccionCollectionTransaccion.setIdProveedor(proveedor);
                transaccionCollectionTransaccion = em.merge(transaccionCollectionTransaccion);
                if (oldIdProveedorOfTransaccionCollectionTransaccion != null) {
                    oldIdProveedorOfTransaccionCollectionTransaccion.getTransaccionCollection().remove(transaccionCollectionTransaccion);
                    oldIdProveedorOfTransaccionCollectionTransaccion = em.merge(oldIdProveedorOfTransaccionCollectionTransaccion);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Proveedor proveedor) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Proveedor persistentProveedor = em.find(Proveedor.class, proveedor.getIdProveedor());
            Municipio idMunicipioPrvOld = persistentProveedor.getIdMunicipioPrv();
            Municipio idMunicipioPrvNew = proveedor.getIdMunicipioPrv();
            Collection<Transaccion> transaccionCollectionOld = persistentProveedor.getTransaccionCollection();
            Collection<Transaccion> transaccionCollectionNew = proveedor.getTransaccionCollection();
            List<String> illegalOrphanMessages = null;
            for (Transaccion transaccionCollectionOldTransaccion : transaccionCollectionOld) {
                if (!transaccionCollectionNew.contains(transaccionCollectionOldTransaccion)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Transaccion " + transaccionCollectionOldTransaccion + " since its idProveedor field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idMunicipioPrvNew != null) {
                idMunicipioPrvNew = em.getReference(idMunicipioPrvNew.getClass(), idMunicipioPrvNew.getIdMunicipio());
                proveedor.setIdMunicipioPrv(idMunicipioPrvNew);
            }
            Collection<Transaccion> attachedTransaccionCollectionNew = new ArrayList<Transaccion>();
            for (Transaccion transaccionCollectionNewTransaccionToAttach : transaccionCollectionNew) {
                transaccionCollectionNewTransaccionToAttach = em.getReference(transaccionCollectionNewTransaccionToAttach.getClass(), transaccionCollectionNewTransaccionToAttach.getIdTransaccion());
                attachedTransaccionCollectionNew.add(transaccionCollectionNewTransaccionToAttach);
            }
            transaccionCollectionNew = attachedTransaccionCollectionNew;
            proveedor.setTransaccionCollection(transaccionCollectionNew);
            proveedor = em.merge(proveedor);
            if (idMunicipioPrvOld != null && !idMunicipioPrvOld.equals(idMunicipioPrvNew)) {
                idMunicipioPrvOld.getProveedorCollection().remove(proveedor);
                idMunicipioPrvOld = em.merge(idMunicipioPrvOld);
            }
            if (idMunicipioPrvNew != null && !idMunicipioPrvNew.equals(idMunicipioPrvOld)) {
                idMunicipioPrvNew.getProveedorCollection().add(proveedor);
                idMunicipioPrvNew = em.merge(idMunicipioPrvNew);
            }
            for (Transaccion transaccionCollectionNewTransaccion : transaccionCollectionNew) {
                if (!transaccionCollectionOld.contains(transaccionCollectionNewTransaccion)) {
                    Proveedor oldIdProveedorOfTransaccionCollectionNewTransaccion = transaccionCollectionNewTransaccion.getIdProveedor();
                    transaccionCollectionNewTransaccion.setIdProveedor(proveedor);
                    transaccionCollectionNewTransaccion = em.merge(transaccionCollectionNewTransaccion);
                    if (oldIdProveedorOfTransaccionCollectionNewTransaccion != null && !oldIdProveedorOfTransaccionCollectionNewTransaccion.equals(proveedor)) {
                        oldIdProveedorOfTransaccionCollectionNewTransaccion.getTransaccionCollection().remove(transaccionCollectionNewTransaccion);
                        oldIdProveedorOfTransaccionCollectionNewTransaccion = em.merge(oldIdProveedorOfTransaccionCollectionNewTransaccion);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = proveedor.getIdProveedor();
                if (findProveedor(id) == null) {
                    throw new NonexistentEntityException("The proveedor with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Proveedor proveedor;
            try {
                proveedor = em.getReference(Proveedor.class, id);
                proveedor.getIdProveedor();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The proveedor with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Transaccion> transaccionCollectionOrphanCheck = proveedor.getTransaccionCollection();
            for (Transaccion transaccionCollectionOrphanCheckTransaccion : transaccionCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Proveedor (" + proveedor + ") cannot be destroyed since the Transaccion " + transaccionCollectionOrphanCheckTransaccion + " in its transaccionCollection field has a non-nullable idProveedor field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Municipio idMunicipioPrv = proveedor.getIdMunicipioPrv();
            if (idMunicipioPrv != null) {
                idMunicipioPrv.getProveedorCollection().remove(proveedor);
                idMunicipioPrv = em.merge(idMunicipioPrv);
            }
            em.remove(proveedor);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Proveedor> findProveedorEntities() {
        return findProveedorEntities(true, -1, -1);
    }

    public List<Proveedor> findProveedorEntities(int maxResults, int firstResult) {
        return findProveedorEntities(false, maxResults, firstResult);
    }

    private List<Proveedor> findProveedorEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Proveedor.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Proveedor findProveedor(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Proveedor.class, id);
        } finally {
            em.close();
        }
    }

    public int getProveedorCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Proveedor> rt = cq.from(Proveedor.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import sv.srn.jpa.entity.Departamento;
import sv.srn.jpa.entity.Proveedor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import sv.srn.jpa.controller.exceptions.NonexistentEntityException;
import sv.srn.jpa.controller.exceptions.PreexistingEntityException;
import sv.srn.jpa.entity.Municipio;

/**
 *
 * @author angel
 */
public class MunicipioJpaController implements Serializable {

    public MunicipioJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Municipio municipio) throws PreexistingEntityException, Exception {
        if (municipio.getProveedorCollection() == null) {
            municipio.setProveedorCollection(new ArrayList<Proveedor>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Departamento idDepartamento = municipio.getIdDepartamento();
            if (idDepartamento != null) {
                idDepartamento = em.getReference(idDepartamento.getClass(), idDepartamento.getIdDepartamento());
                municipio.setIdDepartamento(idDepartamento);
            }
            Collection<Proveedor> attachedProveedorCollection = new ArrayList<Proveedor>();
            for (Proveedor proveedorCollectionProveedorToAttach : municipio.getProveedorCollection()) {
                proveedorCollectionProveedorToAttach = em.getReference(proveedorCollectionProveedorToAttach.getClass(), proveedorCollectionProveedorToAttach.getIdProveedor());
                attachedProveedorCollection.add(proveedorCollectionProveedorToAttach);
            }
            municipio.setProveedorCollection(attachedProveedorCollection);
            em.persist(municipio);
            if (idDepartamento != null) {
                idDepartamento.getMunicipioCollection().add(municipio);
                idDepartamento = em.merge(idDepartamento);
            }
            for (Proveedor proveedorCollectionProveedor : municipio.getProveedorCollection()) {
                Municipio oldIdMunicipioPrvOfProveedorCollectionProveedor = proveedorCollectionProveedor.getIdMunicipioPrv();
                proveedorCollectionProveedor.setIdMunicipioPrv(municipio);
                proveedorCollectionProveedor = em.merge(proveedorCollectionProveedor);
                if (oldIdMunicipioPrvOfProveedorCollectionProveedor != null) {
                    oldIdMunicipioPrvOfProveedorCollectionProveedor.getProveedorCollection().remove(proveedorCollectionProveedor);
                    oldIdMunicipioPrvOfProveedorCollectionProveedor = em.merge(oldIdMunicipioPrvOfProveedorCollectionProveedor);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMunicipio(municipio.getIdMunicipio()) != null) {
                throw new PreexistingEntityException("Municipio " + municipio + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Municipio municipio) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Municipio persistentMunicipio = em.find(Municipio.class, municipio.getIdMunicipio());
            Departamento idDepartamentoOld = persistentMunicipio.getIdDepartamento();
            Departamento idDepartamentoNew = municipio.getIdDepartamento();
            Collection<Proveedor> proveedorCollectionOld = persistentMunicipio.getProveedorCollection();
            Collection<Proveedor> proveedorCollectionNew = municipio.getProveedorCollection();
            if (idDepartamentoNew != null) {
                idDepartamentoNew = em.getReference(idDepartamentoNew.getClass(), idDepartamentoNew.getIdDepartamento());
                municipio.setIdDepartamento(idDepartamentoNew);
            }
            Collection<Proveedor> attachedProveedorCollectionNew = new ArrayList<Proveedor>();
            for (Proveedor proveedorCollectionNewProveedorToAttach : proveedorCollectionNew) {
                proveedorCollectionNewProveedorToAttach = em.getReference(proveedorCollectionNewProveedorToAttach.getClass(), proveedorCollectionNewProveedorToAttach.getIdProveedor());
                attachedProveedorCollectionNew.add(proveedorCollectionNewProveedorToAttach);
            }
            proveedorCollectionNew = attachedProveedorCollectionNew;
            municipio.setProveedorCollection(proveedorCollectionNew);
            municipio = em.merge(municipio);
            if (idDepartamentoOld != null && !idDepartamentoOld.equals(idDepartamentoNew)) {
                idDepartamentoOld.getMunicipioCollection().remove(municipio);
                idDepartamentoOld = em.merge(idDepartamentoOld);
            }
            if (idDepartamentoNew != null && !idDepartamentoNew.equals(idDepartamentoOld)) {
                idDepartamentoNew.getMunicipioCollection().add(municipio);
                idDepartamentoNew = em.merge(idDepartamentoNew);
            }
            for (Proveedor proveedorCollectionOldProveedor : proveedorCollectionOld) {
                if (!proveedorCollectionNew.contains(proveedorCollectionOldProveedor)) {
                    proveedorCollectionOldProveedor.setIdMunicipioPrv(null);
                    proveedorCollectionOldProveedor = em.merge(proveedorCollectionOldProveedor);
                }
            }
            for (Proveedor proveedorCollectionNewProveedor : proveedorCollectionNew) {
                if (!proveedorCollectionOld.contains(proveedorCollectionNewProveedor)) {
                    Municipio oldIdMunicipioPrvOfProveedorCollectionNewProveedor = proveedorCollectionNewProveedor.getIdMunicipioPrv();
                    proveedorCollectionNewProveedor.setIdMunicipioPrv(municipio);
                    proveedorCollectionNewProveedor = em.merge(proveedorCollectionNewProveedor);
                    if (oldIdMunicipioPrvOfProveedorCollectionNewProveedor != null && !oldIdMunicipioPrvOfProveedorCollectionNewProveedor.equals(municipio)) {
                        oldIdMunicipioPrvOfProveedorCollectionNewProveedor.getProveedorCollection().remove(proveedorCollectionNewProveedor);
                        oldIdMunicipioPrvOfProveedorCollectionNewProveedor = em.merge(oldIdMunicipioPrvOfProveedorCollectionNewProveedor);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = municipio.getIdMunicipio();
                if (findMunicipio(id) == null) {
                    throw new NonexistentEntityException("The municipio with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Municipio municipio;
            try {
                municipio = em.getReference(Municipio.class, id);
                municipio.getIdMunicipio();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The municipio with id " + id + " no longer exists.", enfe);
            }
            Departamento idDepartamento = municipio.getIdDepartamento();
            if (idDepartamento != null) {
                idDepartamento.getMunicipioCollection().remove(municipio);
                idDepartamento = em.merge(idDepartamento);
            }
            Collection<Proveedor> proveedorCollection = municipio.getProveedorCollection();
            for (Proveedor proveedorCollectionProveedor : proveedorCollection) {
                proveedorCollectionProveedor.setIdMunicipioPrv(null);
                proveedorCollectionProveedor = em.merge(proveedorCollectionProveedor);
            }
            em.remove(municipio);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Municipio> findMunicipioEntities() {
        return findMunicipioEntities(true, -1, -1);
    }

    public List<Municipio> findMunicipioEntities(int maxResults, int firstResult) {
        return findMunicipioEntities(false, maxResults, firstResult);
    }

    private List<Municipio> findMunicipioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Municipio.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Municipio findMunicipio(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Municipio.class, id);
        } finally {
            em.close();
        }
    }

    public int getMunicipioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Municipio> rt = cq.from(Municipio.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

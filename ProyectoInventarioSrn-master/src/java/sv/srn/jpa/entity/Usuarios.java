/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel
 */
@Entity
@Table(name = "usuarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuarios.findAll", query = "SELECT u FROM Usuarios u")
    , @NamedQuery(name = "Usuarios.findByIdUsuario", query = "SELECT u FROM Usuarios u WHERE u.idUsuario = :idUsuario")
    , @NamedQuery(name = "Usuarios.findByCodigoUsu", query = "SELECT u FROM Usuarios u WHERE u.codigoUsu = :codigoUsu")
    , @NamedQuery(name = "Usuarios.findByNombreUsu", query = "SELECT u FROM Usuarios u WHERE u.nombreUsu = :nombreUsu")
    , @NamedQuery(name = "Usuarios.findByDireccionUsu", query = "SELECT u FROM Usuarios u WHERE u.direccionUsu = :direccionUsu")
    , @NamedQuery(name = "Usuarios.findByTelefonoUsu", query = "SELECT u FROM Usuarios u WHERE u.telefonoUsu = :telefonoUsu")
    , @NamedQuery(name = "Usuarios.findByEmailUsu", query = "SELECT u FROM Usuarios u WHERE u.emailUsu = :emailUsu")
    , @NamedQuery(name = "Usuarios.findByDuiUsu", query = "SELECT u FROM Usuarios u WHERE u.duiUsu = :duiUsu")
    , @NamedQuery(name = "Usuarios.findByNitUsu", query = "SELECT u FROM Usuarios u WHERE u.nitUsu = :nitUsu")
    , @NamedQuery(name = "Usuarios.findByFechaCreacionUsu", query = "SELECT u FROM Usuarios u WHERE u.fechaCreacionUsu = :fechaCreacionUsu")
    , @NamedQuery(name = "Usuarios.findByUsuarioCreacionUsu", query = "SELECT u FROM Usuarios u WHERE u.usuarioCreacionUsu = :usuarioCreacionUsu")
    , @NamedQuery(name = "Usuarios.findByFechaModificacionTra", query = "SELECT u FROM Usuarios u WHERE u.fechaModificacionTra = :fechaModificacionTra")
    , @NamedQuery(name = "Usuarios.findByUsuarioModificacinTra", query = "SELECT u FROM Usuarios u WHERE u.usuarioModificacinTra = :usuarioModificacinTra")})
public class Usuarios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_usuario")
    private Integer idUsuario;
    @Column(name = "codigo_usu")
    private String codigoUsu;
    @Basic(optional = false)
    @Column(name = "nombre_usu")
    private String nombreUsu;
    @Basic(optional = false)
    @Column(name = "direccion_usu")
    private String direccionUsu;
    @Basic(optional = false)
    @Column(name = "telefono_usu")
    private int telefonoUsu;
    @Basic(optional = false)
    @Column(name = "email_usu")
    private String emailUsu;
    @Column(name = "dui_usu")
    private String duiUsu;
    @Column(name = "nit_usu")
    private String nitUsu;
    @Basic(optional = false)
    @Column(name = "fecha_creacion_usu")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacionUsu;
    @Basic(optional = false)
    @Column(name = "usuario_creacion_usu")
    private int usuarioCreacionUsu;
    @Basic(optional = false)
    @Column(name = "fecha_modificacion_tra")
    @Temporal(TemporalType.DATE)
    private Date fechaModificacionTra;
    @Basic(optional = false)
    @Column(name = "usuario_modificacin_tra")
    private int usuarioModificacinTra;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUsuario")
    private Collection<Transaccion> transaccionCollection;

    public Usuarios() {
    }

    public Usuarios(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Usuarios(Integer idUsuario, String nombreUsu, String direccionUsu, int telefonoUsu, String emailUsu, Date fechaCreacionUsu, int usuarioCreacionUsu, Date fechaModificacionTra, int usuarioModificacinTra) {
        this.idUsuario = idUsuario;
        this.nombreUsu = nombreUsu;
        this.direccionUsu = direccionUsu;
        this.telefonoUsu = telefonoUsu;
        this.emailUsu = emailUsu;
        this.fechaCreacionUsu = fechaCreacionUsu;
        this.usuarioCreacionUsu = usuarioCreacionUsu;
        this.fechaModificacionTra = fechaModificacionTra;
        this.usuarioModificacinTra = usuarioModificacinTra;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getCodigoUsu() {
        return codigoUsu;
    }

    public void setCodigoUsu(String codigoUsu) {
        this.codigoUsu = codigoUsu;
    }

    public String getNombreUsu() {
        return nombreUsu;
    }

    public void setNombreUsu(String nombreUsu) {
        this.nombreUsu = nombreUsu;
    }

    public String getDireccionUsu() {
        return direccionUsu;
    }

    public void setDireccionUsu(String direccionUsu) {
        this.direccionUsu = direccionUsu;
    }

    public int getTelefonoUsu() {
        return telefonoUsu;
    }

    public void setTelefonoUsu(int telefonoUsu) {
        this.telefonoUsu = telefonoUsu;
    }

    public String getEmailUsu() {
        return emailUsu;
    }

    public void setEmailUsu(String emailUsu) {
        this.emailUsu = emailUsu;
    }

    public String getDuiUsu() {
        return duiUsu;
    }

    public void setDuiUsu(String duiUsu) {
        this.duiUsu = duiUsu;
    }

    public String getNitUsu() {
        return nitUsu;
    }

    public void setNitUsu(String nitUsu) {
        this.nitUsu = nitUsu;
    }

    public Date getFechaCreacionUsu() {
        return fechaCreacionUsu;
    }

    public void setFechaCreacionUsu(Date fechaCreacionUsu) {
        this.fechaCreacionUsu = fechaCreacionUsu;
    }

    public int getUsuarioCreacionUsu() {
        return usuarioCreacionUsu;
    }

    public void setUsuarioCreacionUsu(int usuarioCreacionUsu) {
        this.usuarioCreacionUsu = usuarioCreacionUsu;
    }

    public Date getFechaModificacionTra() {
        return fechaModificacionTra;
    }

    public void setFechaModificacionTra(Date fechaModificacionTra) {
        this.fechaModificacionTra = fechaModificacionTra;
    }

    public int getUsuarioModificacinTra() {
        return usuarioModificacinTra;
    }

    public void setUsuarioModificacinTra(int usuarioModificacinTra) {
        this.usuarioModificacinTra = usuarioModificacinTra;
    }

    @XmlTransient
    public Collection<Transaccion> getTransaccionCollection() {
        return transaccionCollection;
    }

    public void setTransaccionCollection(Collection<Transaccion> transaccionCollection) {
        this.transaccionCollection = transaccionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuario != null ? idUsuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuarios)) {
            return false;
        }
        Usuarios other = (Usuarios) object;
        if ((this.idUsuario == null && other.idUsuario != null) || (this.idUsuario != null && !this.idUsuario.equals(other.idUsuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.srn.jpa.entity.Usuarios[ idUsuario=" + idUsuario + " ]";
    }
    
}

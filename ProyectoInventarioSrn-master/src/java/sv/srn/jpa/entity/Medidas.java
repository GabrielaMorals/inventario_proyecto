/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel
 */
@Entity
@Table(name = "medidas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Medidas.findAll", query = "SELECT m FROM Medidas m")
    , @NamedQuery(name = "Medidas.findByIdMedida", query = "SELECT m FROM Medidas m WHERE m.idMedida = :idMedida")
    , @NamedQuery(name = "Medidas.findByCodigoMed", query = "SELECT m FROM Medidas m WHERE m.codigoMed = :codigoMed")
    , @NamedQuery(name = "Medidas.findByValorMed", query = "SELECT m FROM Medidas m WHERE m.valorMed = :valorMed")
    , @NamedQuery(name = "Medidas.findByLonaMed", query = "SELECT m FROM Medidas m WHERE m.lonaMed = :lonaMed")
    , @NamedQuery(name = "Medidas.findByFechaCreacionMed", query = "SELECT m FROM Medidas m WHERE m.fechaCreacionMed = :fechaCreacionMed")
    , @NamedQuery(name = "Medidas.findByUsuarioCreacionMed", query = "SELECT m FROM Medidas m WHERE m.usuarioCreacionMed = :usuarioCreacionMed")
    , @NamedQuery(name = "Medidas.findByFechaModificacionMed", query = "SELECT m FROM Medidas m WHERE m.fechaModificacionMed = :fechaModificacionMed")
    , @NamedQuery(name = "Medidas.findByUsuModificacionMed", query = "SELECT m FROM Medidas m WHERE m.usuModificacionMed = :usuModificacionMed")})
public class Medidas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_medida")
    private Integer idMedida;
    @Basic(optional = false)
    @Column(name = "codigo_med")
    private String codigoMed;
    @Basic(optional = false)
    @Column(name = "valor_med")
    private String valorMed;
    @Basic(optional = false)
    @Column(name = "lona_med")
    private String lonaMed;
    @Basic(optional = false)
    @Column(name = "fecha_creacion_med")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacionMed;
    @Basic(optional = false)
    @Column(name = "usuario_creacion_med")
    private int usuarioCreacionMed;
    @Basic(optional = false)
    @Column(name = "fecha_modificacion_med")
    @Temporal(TemporalType.DATE)
    private Date fechaModificacionMed;
    @Basic(optional = false)
    @Column(name = "usu_modificacion_med")
    private int usuModificacionMed;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMedida")
    private Collection<Producto> productoCollection;

    public Medidas() {
    }

    public Medidas(Integer idMedida) {
        this.idMedida = idMedida;
    }

    public Medidas(Integer idMedida, String codigoMed, String valorMed, String lonaMed, Date fechaCreacionMed, int usuarioCreacionMed, Date fechaModificacionMed, int usuModificacionMed) {
        this.idMedida = idMedida;
        this.codigoMed = codigoMed;
        this.valorMed = valorMed;
        this.lonaMed = lonaMed;
        this.fechaCreacionMed = fechaCreacionMed;
        this.usuarioCreacionMed = usuarioCreacionMed;
        this.fechaModificacionMed = fechaModificacionMed;
        this.usuModificacionMed = usuModificacionMed;
    }

    public Integer getIdMedida() {
        return idMedida;
    }

    public void setIdMedida(Integer idMedida) {
        this.idMedida = idMedida;
    }

    public String getCodigoMed() {
        return codigoMed;
    }

    public void setCodigoMed(String codigoMed) {
        this.codigoMed = codigoMed;
    }

    public String getValorMed() {
        return valorMed;
    }

    public void setValorMed(String valorMed) {
        this.valorMed = valorMed;
    }

    public String getLonaMed() {
        return lonaMed;
    }

    public void setLonaMed(String lonaMed) {
        this.lonaMed = lonaMed;
    }

    public Date getFechaCreacionMed() {
        return fechaCreacionMed;
    }

    public void setFechaCreacionMed(Date fechaCreacionMed) {
        this.fechaCreacionMed = fechaCreacionMed;
    }

    public int getUsuarioCreacionMed() {
        return usuarioCreacionMed;
    }

    public void setUsuarioCreacionMed(int usuarioCreacionMed) {
        this.usuarioCreacionMed = usuarioCreacionMed;
    }

    public Date getFechaModificacionMed() {
        return fechaModificacionMed;
    }

    public void setFechaModificacionMed(Date fechaModificacionMed) {
        this.fechaModificacionMed = fechaModificacionMed;
    }

    public int getUsuModificacionMed() {
        return usuModificacionMed;
    }

    public void setUsuModificacionMed(int usuModificacionMed) {
        this.usuModificacionMed = usuModificacionMed;
    }

    @XmlTransient
    public Collection<Producto> getProductoCollection() {
        return productoCollection;
    }

    public void setProductoCollection(Collection<Producto> productoCollection) {
        this.productoCollection = productoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMedida != null ? idMedida.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Medidas)) {
            return false;
        }
        Medidas other = (Medidas) object;
        if ((this.idMedida == null && other.idMedida != null) || (this.idMedida != null && !this.idMedida.equals(other.idMedida))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.srn.jpa.entity.Medidas[ idMedida=" + idMedida + " ]";
    }
    
}

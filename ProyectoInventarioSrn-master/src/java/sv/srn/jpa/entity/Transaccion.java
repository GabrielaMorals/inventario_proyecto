/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author angel
 */
@Entity
@Table(name = "transaccion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Transaccion.findAll", query = "SELECT t FROM Transaccion t")
    , @NamedQuery(name = "Transaccion.findByIdTransaccion", query = "SELECT t FROM Transaccion t WHERE t.idTransaccion = :idTransaccion")
    , @NamedQuery(name = "Transaccion.findByCantidadTra", query = "SELECT t FROM Transaccion t WHERE t.cantidadTra = :cantidadTra")
    , @NamedQuery(name = "Transaccion.findByCostoUnitarioTra", query = "SELECT t FROM Transaccion t WHERE t.costoUnitarioTra = :costoUnitarioTra")
    , @NamedQuery(name = "Transaccion.findByFechaCreacionTra", query = "SELECT t FROM Transaccion t WHERE t.fechaCreacionTra = :fechaCreacionTra")
    , @NamedQuery(name = "Transaccion.findByUsuarioCreacionTra", query = "SELECT t FROM Transaccion t WHERE t.usuarioCreacionTra = :usuarioCreacionTra")
    , @NamedQuery(name = "Transaccion.findByFechaModificacionTra", query = "SELECT t FROM Transaccion t WHERE t.fechaModificacionTra = :fechaModificacionTra")
    , @NamedQuery(name = "Transaccion.findByUsuarioModificacionTra", query = "SELECT t FROM Transaccion t WHERE t.usuarioModificacionTra = :usuarioModificacionTra")})
public class Transaccion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_transaccion")
    private Integer idTransaccion;
    @Basic(optional = false)
    @Column(name = "cantidad_tra")
    private int cantidadTra;
    @Basic(optional = false)
    @Column(name = "costo_unitario_tra")
    private short costoUnitarioTra;
    @Basic(optional = false)
    @Column(name = "fecha_creacion_tra")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacionTra;
    @Basic(optional = false)
    @Column(name = "usuario_creacion_tra")
    private int usuarioCreacionTra;
    @Basic(optional = false)
    @Column(name = "fecha_modificacion_tra")
    @Temporal(TemporalType.DATE)
    private Date fechaModificacionTra;
    @Basic(optional = false)
    @Column(name = "usuario_modificacion_tra")
    private int usuarioModificacionTra;
    @JoinColumn(name = "id_bodega", referencedColumnName = "id_bodega")
    @ManyToOne(optional = false)
    private Bodega idBodega;
    @JoinColumn(name = "id_producto", referencedColumnName = "id_producto")
    @ManyToOne(optional = false)
    private Producto idProducto;
    @JoinColumn(name = "id_proveedor", referencedColumnName = "id_proveedor")
    @ManyToOne(optional = false)
    private Proveedor idProveedor;
    @JoinColumn(name = "id_tipo_transaccion", referencedColumnName = "id_tipo_tran")
    @ManyToOne(optional = false)
    private TipoTransaccion idTipoTransaccion;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private Usuarios idUsuario;

    public Transaccion() {
    }

    public Transaccion(Integer idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public Transaccion(Integer idTransaccion, int cantidadTra, short costoUnitarioTra, Date fechaCreacionTra, int usuarioCreacionTra, Date fechaModificacionTra, int usuarioModificacionTra) {
        this.idTransaccion = idTransaccion;
        this.cantidadTra = cantidadTra;
        this.costoUnitarioTra = costoUnitarioTra;
        this.fechaCreacionTra = fechaCreacionTra;
        this.usuarioCreacionTra = usuarioCreacionTra;
        this.fechaModificacionTra = fechaModificacionTra;
        this.usuarioModificacionTra = usuarioModificacionTra;
    }

    public Integer getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(Integer idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public int getCantidadTra() {
        return cantidadTra;
    }

    public void setCantidadTra(int cantidadTra) {
        this.cantidadTra = cantidadTra;
    }

    public short getCostoUnitarioTra() {
        return costoUnitarioTra;
    }

    public void setCostoUnitarioTra(short costoUnitarioTra) {
        this.costoUnitarioTra = costoUnitarioTra;
    }

    public Date getFechaCreacionTra() {
        return fechaCreacionTra;
    }

    public void setFechaCreacionTra(Date fechaCreacionTra) {
        this.fechaCreacionTra = fechaCreacionTra;
    }

    public int getUsuarioCreacionTra() {
        return usuarioCreacionTra;
    }

    public void setUsuarioCreacionTra(int usuarioCreacionTra) {
        this.usuarioCreacionTra = usuarioCreacionTra;
    }

    public Date getFechaModificacionTra() {
        return fechaModificacionTra;
    }

    public void setFechaModificacionTra(Date fechaModificacionTra) {
        this.fechaModificacionTra = fechaModificacionTra;
    }

    public int getUsuarioModificacionTra() {
        return usuarioModificacionTra;
    }

    public void setUsuarioModificacionTra(int usuarioModificacionTra) {
        this.usuarioModificacionTra = usuarioModificacionTra;
    }

    public Bodega getIdBodega() {
        return idBodega;
    }

    public void setIdBodega(Bodega idBodega) {
        this.idBodega = idBodega;
    }

    public Producto getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Producto idProducto) {
        this.idProducto = idProducto;
    }

    public Proveedor getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Proveedor idProveedor) {
        this.idProveedor = idProveedor;
    }

    public TipoTransaccion getIdTipoTransaccion() {
        return idTipoTransaccion;
    }

    public void setIdTipoTransaccion(TipoTransaccion idTipoTransaccion) {
        this.idTipoTransaccion = idTipoTransaccion;
    }

    public Usuarios getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuarios idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTransaccion != null ? idTransaccion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transaccion)) {
            return false;
        }
        Transaccion other = (Transaccion) object;
        if ((this.idTransaccion == null && other.idTransaccion != null) || (this.idTransaccion != null && !this.idTransaccion.equals(other.idTransaccion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.srn.jpa.entity.Transaccion[ idTransaccion=" + idTransaccion + " ]";
    }
    
}

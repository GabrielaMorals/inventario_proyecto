/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel
 */
@Entity
@Table(name = "proveedor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proveedor.findAll", query = "SELECT p FROM Proveedor p")
    , @NamedQuery(name = "Proveedor.findByIdProveedor", query = "SELECT p FROM Proveedor p WHERE p.idProveedor = :idProveedor")
    , @NamedQuery(name = "Proveedor.findByCodigoPrv", query = "SELECT p FROM Proveedor p WHERE p.codigoPrv = :codigoPrv")
    , @NamedQuery(name = "Proveedor.findByNombrePrv", query = "SELECT p FROM Proveedor p WHERE p.nombrePrv = :nombrePrv")
    , @NamedQuery(name = "Proveedor.findByDireccionPrv", query = "SELECT p FROM Proveedor p WHERE p.direccionPrv = :direccionPrv")
    , @NamedQuery(name = "Proveedor.findByTelefonoPrv", query = "SELECT p FROM Proveedor p WHERE p.telefonoPrv = :telefonoPrv")
    , @NamedQuery(name = "Proveedor.findByFechaCreacionPrv", query = "SELECT p FROM Proveedor p WHERE p.fechaCreacionPrv = :fechaCreacionPrv")
    , @NamedQuery(name = "Proveedor.findByUsuarioCreacionPrv", query = "SELECT p FROM Proveedor p WHERE p.usuarioCreacionPrv = :usuarioCreacionPrv")
    , @NamedQuery(name = "Proveedor.findByFechaModificacionPrv", query = "SELECT p FROM Proveedor p WHERE p.fechaModificacionPrv = :fechaModificacionPrv")
    , @NamedQuery(name = "Proveedor.findByUsuModificacionPrv", query = "SELECT p FROM Proveedor p WHERE p.usuModificacionPrv = :usuModificacionPrv")})
public class Proveedor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_proveedor")
    private Integer idProveedor;
    @Basic(optional = false)
    @Column(name = "codigo_prv")
    private String codigoPrv;
    @Basic(optional = false)
    @Column(name = "nombre_prv")
    private String nombrePrv;
    @Basic(optional = false)
    @Column(name = "direccion_prv")
    private String direccionPrv;
    @Basic(optional = false)
    @Column(name = "telefono_prv")
    private String telefonoPrv;
    @Basic(optional = false)
    @Column(name = "fecha_creacion_prv")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacionPrv;
    @Basic(optional = false)
    @Column(name = "usuario_creacion_prv")
    private int usuarioCreacionPrv;
    @Basic(optional = false)
    @Column(name = "fecha_modificacion_prv")
    @Temporal(TemporalType.DATE)
    private Date fechaModificacionPrv;
    @Basic(optional = false)
    @Column(name = "usu_modificacion_prv")
    private int usuModificacionPrv;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProveedor")
    private Collection<Transaccion> transaccionCollection;
    @JoinColumn(name = "id_municipio_prv", referencedColumnName = "id_municipio")
    @ManyToOne
    private Municipio idMunicipioPrv;

    public Proveedor() {
    }

    public Proveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    public Proveedor(Integer idProveedor, String codigoPrv, String nombrePrv, String direccionPrv, String telefonoPrv, Date fechaCreacionPrv, int usuarioCreacionPrv, Date fechaModificacionPrv, int usuModificacionPrv) {
        this.idProveedor = idProveedor;
        this.codigoPrv = codigoPrv;
        this.nombrePrv = nombrePrv;
        this.direccionPrv = direccionPrv;
        this.telefonoPrv = telefonoPrv;
        this.fechaCreacionPrv = fechaCreacionPrv;
        this.usuarioCreacionPrv = usuarioCreacionPrv;
        this.fechaModificacionPrv = fechaModificacionPrv;
        this.usuModificacionPrv = usuModificacionPrv;
    }

    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getCodigoPrv() {
        return codigoPrv;
    }

    public void setCodigoPrv(String codigoPrv) {
        this.codigoPrv = codigoPrv;
    }

    public String getNombrePrv() {
        return nombrePrv;
    }

    public void setNombrePrv(String nombrePrv) {
        this.nombrePrv = nombrePrv;
    }

    public String getDireccionPrv() {
        return direccionPrv;
    }

    public void setDireccionPrv(String direccionPrv) {
        this.direccionPrv = direccionPrv;
    }

    public String getTelefonoPrv() {
        return telefonoPrv;
    }

    public void setTelefonoPrv(String telefonoPrv) {
        this.telefonoPrv = telefonoPrv;
    }

    public Date getFechaCreacionPrv() {
        return fechaCreacionPrv;
    }

    public void setFechaCreacionPrv(Date fechaCreacionPrv) {
        this.fechaCreacionPrv = fechaCreacionPrv;
    }

    public int getUsuarioCreacionPrv() {
        return usuarioCreacionPrv;
    }

    public void setUsuarioCreacionPrv(int usuarioCreacionPrv) {
        this.usuarioCreacionPrv = usuarioCreacionPrv;
    }

    public Date getFechaModificacionPrv() {
        return fechaModificacionPrv;
    }

    public void setFechaModificacionPrv(Date fechaModificacionPrv) {
        this.fechaModificacionPrv = fechaModificacionPrv;
    }

    public int getUsuModificacionPrv() {
        return usuModificacionPrv;
    }

    public void setUsuModificacionPrv(int usuModificacionPrv) {
        this.usuModificacionPrv = usuModificacionPrv;
    }

    @XmlTransient
    public Collection<Transaccion> getTransaccionCollection() {
        return transaccionCollection;
    }

    public void setTransaccionCollection(Collection<Transaccion> transaccionCollection) {
        this.transaccionCollection = transaccionCollection;
    }

    public Municipio getIdMunicipioPrv() {
        return idMunicipioPrv;
    }

    public void setIdMunicipioPrv(Municipio idMunicipioPrv) {
        this.idMunicipioPrv = idMunicipioPrv;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProveedor != null ? idProveedor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proveedor)) {
            return false;
        }
        Proveedor other = (Proveedor) object;
        if ((this.idProveedor == null && other.idProveedor != null) || (this.idProveedor != null && !this.idProveedor.equals(other.idProveedor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.srn.jpa.entity.Proveedor[ idProveedor=" + idProveedor + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel
 */
@Entity
@Table(name = "bodega")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bodega.findAll", query = "SELECT b FROM Bodega b")
    , @NamedQuery(name = "Bodega.findByIdBodega", query = "SELECT b FROM Bodega b WHERE b.idBodega = :idBodega")
    , @NamedQuery(name = "Bodega.findByCodigoBod", query = "SELECT b FROM Bodega b WHERE b.codigoBod = :codigoBod")
    , @NamedQuery(name = "Bodega.findByNombreBod", query = "SELECT b FROM Bodega b WHERE b.nombreBod = :nombreBod")
    , @NamedQuery(name = "Bodega.findByUbicacionBod", query = "SELECT b FROM Bodega b WHERE b.ubicacionBod = :ubicacionBod")
    , @NamedQuery(name = "Bodega.findByFechaCreacionBod", query = "SELECT b FROM Bodega b WHERE b.fechaCreacionBod = :fechaCreacionBod")
    , @NamedQuery(name = "Bodega.findByUsuarioCreacionBod", query = "SELECT b FROM Bodega b WHERE b.usuarioCreacionBod = :usuarioCreacionBod")
    , @NamedQuery(name = "Bodega.findByFechaModificacionBod", query = "SELECT b FROM Bodega b WHERE b.fechaModificacionBod = :fechaModificacionBod")
    , @NamedQuery(name = "Bodega.findByUsuModificacionBod", query = "SELECT b FROM Bodega b WHERE b.usuModificacionBod = :usuModificacionBod")})
public class Bodega implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_bodega")
    private Integer idBodega;
    @Basic(optional = false)
    @Column(name = "codigo_bod")
    private String codigoBod;
    @Basic(optional = false)
    @Column(name = "nombre_bod")
    private String nombreBod;
    @Basic(optional = false)
    @Column(name = "ubicacion_bod")
    private String ubicacionBod;
    @Basic(optional = false)
    @Column(name = "fecha_creacion_bod")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacionBod;
    @Basic(optional = false)
    @Column(name = "usuario_creacion_bod")
    private int usuarioCreacionBod;
    @Basic(optional = false)
    @Column(name = "fecha_modificacion_bod")
    @Temporal(TemporalType.DATE)
    private Date fechaModificacionBod;
    @Basic(optional = false)
    @Column(name = "usu_modificacion_bod")
    private int usuModificacionBod;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idBodega")
    private Collection<Transaccion> transaccionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idBodegaBxp")
    private Collection<BodegaExistencia> bodegaExistenciaCollection;

    public Bodega() {
    }

    public Bodega(Integer idBodega) {
        this.idBodega = idBodega;
    }

    public Bodega(Integer idBodega, String codigoBod, String nombreBod, String ubicacionBod, Date fechaCreacionBod, int usuarioCreacionBod, Date fechaModificacionBod, int usuModificacionBod) {
        this.idBodega = idBodega;
        this.codigoBod = codigoBod;
        this.nombreBod = nombreBod;
        this.ubicacionBod = ubicacionBod;
        this.fechaCreacionBod = fechaCreacionBod;
        this.usuarioCreacionBod = usuarioCreacionBod;
        this.fechaModificacionBod = fechaModificacionBod;
        this.usuModificacionBod = usuModificacionBod;
    }

    public Integer getIdBodega() {
        return idBodega;
    }

    public void setIdBodega(Integer idBodega) {
        this.idBodega = idBodega;
    }

    public String getCodigoBod() {
        return codigoBod;
    }

    public void setCodigoBod(String codigoBod) {
        this.codigoBod = codigoBod;
    }

    public String getNombreBod() {
        return nombreBod;
    }

    public void setNombreBod(String nombreBod) {
        this.nombreBod = nombreBod;
    }

    public String getUbicacionBod() {
        return ubicacionBod;
    }

    public void setUbicacionBod(String ubicacionBod) {
        this.ubicacionBod = ubicacionBod;
    }

    public Date getFechaCreacionBod() {
        return fechaCreacionBod;
    }

    public void setFechaCreacionBod(Date fechaCreacionBod) {
        this.fechaCreacionBod = fechaCreacionBod;
    }

    public int getUsuarioCreacionBod() {
        return usuarioCreacionBod;
    }

    public void setUsuarioCreacionBod(int usuarioCreacionBod) {
        this.usuarioCreacionBod = usuarioCreacionBod;
    }

    public Date getFechaModificacionBod() {
        return fechaModificacionBod;
    }

    public void setFechaModificacionBod(Date fechaModificacionBod) {
        this.fechaModificacionBod = fechaModificacionBod;
    }

    public int getUsuModificacionBod() {
        return usuModificacionBod;
    }

    public void setUsuModificacionBod(int usuModificacionBod) {
        this.usuModificacionBod = usuModificacionBod;
    }

    @XmlTransient
    public Collection<Transaccion> getTransaccionCollection() {
        return transaccionCollection;
    }

    public void setTransaccionCollection(Collection<Transaccion> transaccionCollection) {
        this.transaccionCollection = transaccionCollection;
    }

    @XmlTransient
    public Collection<BodegaExistencia> getBodegaExistenciaCollection() {
        return bodegaExistenciaCollection;
    }

    public void setBodegaExistenciaCollection(Collection<BodegaExistencia> bodegaExistenciaCollection) {
        this.bodegaExistenciaCollection = bodegaExistenciaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idBodega != null ? idBodega.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bodega)) {
            return false;
        }
        Bodega other = (Bodega) object;
        if ((this.idBodega == null && other.idBodega != null) || (this.idBodega != null && !this.idBodega.equals(other.idBodega))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.srn.jpa.entity.Bodega[ idBodega=" + idBodega + " ]";
    }
    
}

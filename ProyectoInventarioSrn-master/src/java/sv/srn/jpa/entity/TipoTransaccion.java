/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel
 */
@Entity
@Table(name = "tipo_transaccion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoTransaccion.findAll", query = "SELECT t FROM TipoTransaccion t")
    , @NamedQuery(name = "TipoTransaccion.findByIdTipoTran", query = "SELECT t FROM TipoTransaccion t WHERE t.idTipoTran = :idTipoTran")
    , @NamedQuery(name = "TipoTransaccion.findByNombreTran", query = "SELECT t FROM TipoTransaccion t WHERE t.nombreTran = :nombreTran")
    , @NamedQuery(name = "TipoTransaccion.findByEfectoTran", query = "SELECT t FROM TipoTransaccion t WHERE t.efectoTran = :efectoTran")
    , @NamedQuery(name = "TipoTransaccion.findByFechaCreacionTran", query = "SELECT t FROM TipoTransaccion t WHERE t.fechaCreacionTran = :fechaCreacionTran")
    , @NamedQuery(name = "TipoTransaccion.findByUsuarioCreacionTran", query = "SELECT t FROM TipoTransaccion t WHERE t.usuarioCreacionTran = :usuarioCreacionTran")
    , @NamedQuery(name = "TipoTransaccion.findByFechaModificacionTran", query = "SELECT t FROM TipoTransaccion t WHERE t.fechaModificacionTran = :fechaModificacionTran")
    , @NamedQuery(name = "TipoTransaccion.findByUsuModificacionTran", query = "SELECT t FROM TipoTransaccion t WHERE t.usuModificacionTran = :usuModificacionTran")})
public class TipoTransaccion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_tipo_tran")
    private Integer idTipoTran;
    @Basic(optional = false)
    @Column(name = "nombre_tran")
    private String nombreTran;
    @Basic(optional = false)
    @Column(name = "efecto_tran")
    private String efectoTran;
    @Basic(optional = false)
    @Column(name = "fecha_creacion_tran")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacionTran;
    @Basic(optional = false)
    @Column(name = "usuario_creacion_tran")
    private int usuarioCreacionTran;
    @Basic(optional = false)
    @Column(name = "fecha_modificacion_tran")
    @Temporal(TemporalType.DATE)
    private Date fechaModificacionTran;
    @Basic(optional = false)
    @Column(name = "usu_modificacion_tran")
    private int usuModificacionTran;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoTransaccion")
    private Collection<Transaccion> transaccionCollection;

    public TipoTransaccion() {
    }

    public TipoTransaccion(Integer idTipoTran) {
        this.idTipoTran = idTipoTran;
    }

    public TipoTransaccion(Integer idTipoTran, String nombreTran, String efectoTran, Date fechaCreacionTran, int usuarioCreacionTran, Date fechaModificacionTran, int usuModificacionTran) {
        this.idTipoTran = idTipoTran;
        this.nombreTran = nombreTran;
        this.efectoTran = efectoTran;
        this.fechaCreacionTran = fechaCreacionTran;
        this.usuarioCreacionTran = usuarioCreacionTran;
        this.fechaModificacionTran = fechaModificacionTran;
        this.usuModificacionTran = usuModificacionTran;
    }

    public Integer getIdTipoTran() {
        return idTipoTran;
    }

    public void setIdTipoTran(Integer idTipoTran) {
        this.idTipoTran = idTipoTran;
    }

    public String getNombreTran() {
        return nombreTran;
    }

    public void setNombreTran(String nombreTran) {
        this.nombreTran = nombreTran;
    }

    public String getEfectoTran() {
        return efectoTran;
    }

    public void setEfectoTran(String efectoTran) {
        this.efectoTran = efectoTran;
    }

    public Date getFechaCreacionTran() {
        return fechaCreacionTran;
    }

    public void setFechaCreacionTran(Date fechaCreacionTran) {
        this.fechaCreacionTran = fechaCreacionTran;
    }

    public int getUsuarioCreacionTran() {
        return usuarioCreacionTran;
    }

    public void setUsuarioCreacionTran(int usuarioCreacionTran) {
        this.usuarioCreacionTran = usuarioCreacionTran;
    }

    public Date getFechaModificacionTran() {
        return fechaModificacionTran;
    }

    public void setFechaModificacionTran(Date fechaModificacionTran) {
        this.fechaModificacionTran = fechaModificacionTran;
    }

    public int getUsuModificacionTran() {
        return usuModificacionTran;
    }

    public void setUsuModificacionTran(int usuModificacionTran) {
        this.usuModificacionTran = usuModificacionTran;
    }

    @XmlTransient
    public Collection<Transaccion> getTransaccionCollection() {
        return transaccionCollection;
    }

    public void setTransaccionCollection(Collection<Transaccion> transaccionCollection) {
        this.transaccionCollection = transaccionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoTran != null ? idTipoTran.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoTransaccion)) {
            return false;
        }
        TipoTransaccion other = (TipoTransaccion) object;
        if ((this.idTipoTran == null && other.idTipoTran != null) || (this.idTipoTran != null && !this.idTipoTran.equals(other.idTipoTran))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.srn.jpa.entity.TipoTransaccion[ idTipoTran=" + idTipoTran + " ]";
    }
    
}

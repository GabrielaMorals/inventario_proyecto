/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel
 */
@Entity
@Table(name = "producto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Producto.findAll", query = "SELECT p FROM Producto p")
    , @NamedQuery(name = "Producto.findByIdProducto", query = "SELECT p FROM Producto p WHERE p.idProducto = :idProducto")
    , @NamedQuery(name = "Producto.findByCodigoPro", query = "SELECT p FROM Producto p WHERE p.codigoPro = :codigoPro")
    , @NamedQuery(name = "Producto.findByNombrePro", query = "SELECT p FROM Producto p WHERE p.nombrePro = :nombrePro")
    , @NamedQuery(name = "Producto.findByCostoUnitarioPro", query = "SELECT p FROM Producto p WHERE p.costoUnitarioPro = :costoUnitarioPro")
    , @NamedQuery(name = "Producto.findByCostoPromedioPro", query = "SELECT p FROM Producto p WHERE p.costoPromedioPro = :costoPromedioPro")
    , @NamedQuery(name = "Producto.findByMarcaPro", query = "SELECT p FROM Producto p WHERE p.marcaPro = :marcaPro")
    , @NamedQuery(name = "Producto.findByDescripcionPro", query = "SELECT p FROM Producto p WHERE p.descripcionPro = :descripcionPro")
    , @NamedQuery(name = "Producto.findByFechaCreacionPro", query = "SELECT p FROM Producto p WHERE p.fechaCreacionPro = :fechaCreacionPro")
    , @NamedQuery(name = "Producto.findByUsuarioCreacionPro", query = "SELECT p FROM Producto p WHERE p.usuarioCreacionPro = :usuarioCreacionPro")
    , @NamedQuery(name = "Producto.findByFechaModificacionPro", query = "SELECT p FROM Producto p WHERE p.fechaModificacionPro = :fechaModificacionPro")
    , @NamedQuery(name = "Producto.findByUsuModificacionPro", query = "SELECT p FROM Producto p WHERE p.usuModificacionPro = :usuModificacionPro")})
public class Producto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_producto")
    private Integer idProducto;
    @Basic(optional = false)
    @Column(name = "codigo_pro")
    private String codigoPro;
    @Basic(optional = false)
    @Column(name = "nombre_pro")
    private String nombrePro;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "costo_unitario_pro")
    private BigDecimal costoUnitarioPro;
    @Basic(optional = false)
    @Column(name = "costo_promedio_pro")
    private BigDecimal costoPromedioPro;
    @Basic(optional = false)
    @Column(name = "marca_pro")
    private String marcaPro;
    @Column(name = "descripcion_pro")
    private String descripcionPro;
    @Basic(optional = false)
    @Column(name = "fecha_creacion_pro")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacionPro;
    @Basic(optional = false)
    @Column(name = "usuario_creacion_pro")
    private int usuarioCreacionPro;
    @Basic(optional = false)
    @Column(name = "fecha_modificacion_pro")
    @Temporal(TemporalType.DATE)
    private Date fechaModificacionPro;
    @Basic(optional = false)
    @Column(name = "usu_modificacion_pro")
    private int usuModificacionPro;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProducto")
    private Collection<Transaccion> transaccionCollection;
    @JoinColumn(name = "id_categoria", referencedColumnName = "id_categoria")
    @ManyToOne(optional = false)
    private Categoria idCategoria;
    @JoinColumn(name = "id_medida", referencedColumnName = "id_medida")
    @ManyToOne(optional = false)
    private Medidas idMedida;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProductoBxp")
    private Collection<BodegaExistencia> bodegaExistenciaCollection;

    public Producto() {
    }

    public Producto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Producto(Integer idProducto, String codigoPro, String nombrePro, BigDecimal costoUnitarioPro, BigDecimal costoPromedioPro, String marcaPro, Date fechaCreacionPro, int usuarioCreacionPro, Date fechaModificacionPro, int usuModificacionPro) {
        this.idProducto = idProducto;
        this.codigoPro = codigoPro;
        this.nombrePro = nombrePro;
        this.costoUnitarioPro = costoUnitarioPro;
        this.costoPromedioPro = costoPromedioPro;
        this.marcaPro = marcaPro;
        this.fechaCreacionPro = fechaCreacionPro;
        this.usuarioCreacionPro = usuarioCreacionPro;
        this.fechaModificacionPro = fechaModificacionPro;
        this.usuModificacionPro = usuModificacionPro;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getCodigoPro() {
        return codigoPro;
    }

    public void setCodigoPro(String codigoPro) {
        this.codigoPro = codigoPro;
    }

    public String getNombrePro() {
        return nombrePro;
    }

    public void setNombrePro(String nombrePro) {
        this.nombrePro = nombrePro;
    }

    public BigDecimal getCostoUnitarioPro() {
        return costoUnitarioPro;
    }

    public void setCostoUnitarioPro(BigDecimal costoUnitarioPro) {
        this.costoUnitarioPro = costoUnitarioPro;
    }

    public BigDecimal getCostoPromedioPro() {
        return costoPromedioPro;
    }

    public void setCostoPromedioPro(BigDecimal costoPromedioPro) {
        this.costoPromedioPro = costoPromedioPro;
    }

    public String getMarcaPro() {
        return marcaPro;
    }

    public void setMarcaPro(String marcaPro) {
        this.marcaPro = marcaPro;
    }

    public String getDescripcionPro() {
        return descripcionPro;
    }

    public void setDescripcionPro(String descripcionPro) {
        this.descripcionPro = descripcionPro;
    }

    public Date getFechaCreacionPro() {
        return fechaCreacionPro;
    }

    public void setFechaCreacionPro(Date fechaCreacionPro) {
        this.fechaCreacionPro = fechaCreacionPro;
    }

    public int getUsuarioCreacionPro() {
        return usuarioCreacionPro;
    }

    public void setUsuarioCreacionPro(int usuarioCreacionPro) {
        this.usuarioCreacionPro = usuarioCreacionPro;
    }

    public Date getFechaModificacionPro() {
        return fechaModificacionPro;
    }

    public void setFechaModificacionPro(Date fechaModificacionPro) {
        this.fechaModificacionPro = fechaModificacionPro;
    }

    public int getUsuModificacionPro() {
        return usuModificacionPro;
    }

    public void setUsuModificacionPro(int usuModificacionPro) {
        this.usuModificacionPro = usuModificacionPro;
    }

    @XmlTransient
    public Collection<Transaccion> getTransaccionCollection() {
        return transaccionCollection;
    }

    public void setTransaccionCollection(Collection<Transaccion> transaccionCollection) {
        this.transaccionCollection = transaccionCollection;
    }

    public Categoria getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Categoria idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Medidas getIdMedida() {
        return idMedida;
    }

    public void setIdMedida(Medidas idMedida) {
        this.idMedida = idMedida;
    }

    @XmlTransient
    public Collection<BodegaExistencia> getBodegaExistenciaCollection() {
        return bodegaExistenciaCollection;
    }

    public void setBodegaExistenciaCollection(Collection<BodegaExistencia> bodegaExistenciaCollection) {
        this.bodegaExistenciaCollection = bodegaExistenciaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProducto != null ? idProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Producto)) {
            return false;
        }
        Producto other = (Producto) object;
        if ((this.idProducto == null && other.idProducto != null) || (this.idProducto != null && !this.idProducto.equals(other.idProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.srn.jpa.entity.Producto[ idProducto=" + idProducto + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author angel
 */
@Entity
@Table(name = "categoria")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Categoria.findAll", query = "SELECT c FROM Categoria c")
    , @NamedQuery(name = "Categoria.findByIdCategoria", query = "SELECT c FROM Categoria c WHERE c.idCategoria = :idCategoria")
    , @NamedQuery(name = "Categoria.findByCodigoCat", query = "SELECT c FROM Categoria c WHERE c.codigoCat = :codigoCat")
    , @NamedQuery(name = "Categoria.findByNombreCat", query = "SELECT c FROM Categoria c WHERE c.nombreCat = :nombreCat")
    , @NamedQuery(name = "Categoria.findByFechaCreacionCat", query = "SELECT c FROM Categoria c WHERE c.fechaCreacionCat = :fechaCreacionCat")
    , @NamedQuery(name = "Categoria.findByUsuarioCreacionCat", query = "SELECT c FROM Categoria c WHERE c.usuarioCreacionCat = :usuarioCreacionCat")
    , @NamedQuery(name = "Categoria.findByFechaModificacionCat", query = "SELECT c FROM Categoria c WHERE c.fechaModificacionCat = :fechaModificacionCat")
    , @NamedQuery(name = "Categoria.findByUsuModificacionCat", query = "SELECT c FROM Categoria c WHERE c.usuModificacionCat = :usuModificacionCat")})
public class Categoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_categoria")
    private Integer idCategoria;
    @Basic(optional = false)
    @Column(name = "codigo_cat")
    private String codigoCat;
    @Basic(optional = false)
    @Column(name = "nombre_cat")
    private String nombreCat;
    @Basic(optional = false)
    @Column(name = "fecha_creacion_cat")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacionCat;
    @Basic(optional = false)
    @Column(name = "usuario_creacion_cat")
    private int usuarioCreacionCat;
    @Basic(optional = false)
    @Column(name = "fecha_modificacion_cat")
    @Temporal(TemporalType.DATE)
    private Date fechaModificacionCat;
    @Basic(optional = false)
    @Column(name = "usu_modificacion_cat")
    private int usuModificacionCat;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCategoria")
    private Collection<Producto> productoCollection;

    public Categoria() {
    }

    public Categoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Categoria(Integer idCategoria, String codigoCat, String nombreCat, Date fechaCreacionCat, int usuarioCreacionCat, Date fechaModificacionCat, int usuModificacionCat) {
        this.idCategoria = idCategoria;
        this.codigoCat = codigoCat;
        this.nombreCat = nombreCat;
        this.fechaCreacionCat = fechaCreacionCat;
        this.usuarioCreacionCat = usuarioCreacionCat;
        this.fechaModificacionCat = fechaModificacionCat;
        this.usuModificacionCat = usuModificacionCat;
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getCodigoCat() {
        return codigoCat;
    }

    public void setCodigoCat(String codigoCat) {
        this.codigoCat = codigoCat;
    }

    public String getNombreCat() {
        return nombreCat;
    }

    public void setNombreCat(String nombreCat) {
        this.nombreCat = nombreCat;
    }

    public Date getFechaCreacionCat() {
        return fechaCreacionCat;
    }

    public void setFechaCreacionCat(Date fechaCreacionCat) {
        this.fechaCreacionCat = fechaCreacionCat;
    }

    public int getUsuarioCreacionCat() {
        return usuarioCreacionCat;
    }

    public void setUsuarioCreacionCat(int usuarioCreacionCat) {
        this.usuarioCreacionCat = usuarioCreacionCat;
    }

    public Date getFechaModificacionCat() {
        return fechaModificacionCat;
    }

    public void setFechaModificacionCat(Date fechaModificacionCat) {
        this.fechaModificacionCat = fechaModificacionCat;
    }

    public int getUsuModificacionCat() {
        return usuModificacionCat;
    }

    public void setUsuModificacionCat(int usuModificacionCat) {
        this.usuModificacionCat = usuModificacionCat;
    }

    @XmlTransient
    public Collection<Producto> getProductoCollection() {
        return productoCollection;
    }

    public void setProductoCollection(Collection<Producto> productoCollection) {
        this.productoCollection = productoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCategoria != null ? idCategoria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Categoria)) {
            return false;
        }
        Categoria other = (Categoria) object;
        if ((this.idCategoria == null && other.idCategoria != null) || (this.idCategoria != null && !this.idCategoria.equals(other.idCategoria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.srn.jpa.entity.Categoria[ idCategoria=" + idCategoria + " ]";
    }
    
}

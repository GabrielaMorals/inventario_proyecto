/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author angel
 */
@Entity
@Table(name = "bodega_existencia")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BodegaExistencia.findAll", query = "SELECT b FROM BodegaExistencia b")
    , @NamedQuery(name = "BodegaExistencia.findByIdbodegaExistencia", query = "SELECT b FROM BodegaExistencia b WHERE b.idbodegaExistencia = :idbodegaExistencia")
    , @NamedQuery(name = "BodegaExistencia.findByCantidadBxp", query = "SELECT b FROM BodegaExistencia b WHERE b.cantidadBxp = :cantidadBxp")
    , @NamedQuery(name = "BodegaExistencia.findByFechaCreacionBxp", query = "SELECT b FROM BodegaExistencia b WHERE b.fechaCreacionBxp = :fechaCreacionBxp")
    , @NamedQuery(name = "BodegaExistencia.findByUsuarioCreacionBxp", query = "SELECT b FROM BodegaExistencia b WHERE b.usuarioCreacionBxp = :usuarioCreacionBxp")
    , @NamedQuery(name = "BodegaExistencia.findByFechaModificacionBxp", query = "SELECT b FROM BodegaExistencia b WHERE b.fechaModificacionBxp = :fechaModificacionBxp")
    , @NamedQuery(name = "BodegaExistencia.findByUsuModificacionBxp", query = "SELECT b FROM BodegaExistencia b WHERE b.usuModificacionBxp = :usuModificacionBxp")})
public class BodegaExistencia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_bodegaExistencia")
    private Integer idbodegaExistencia;
    @Column(name = "cantidad_bxp")
    private Short cantidadBxp;
    @Basic(optional = false)
    @Column(name = "fecha_creacion_bxp")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacionBxp;
    @Basic(optional = false)
    @Column(name = "usuario_creacion_bxp")
    private int usuarioCreacionBxp;
    @Basic(optional = false)
    @Column(name = "fecha_modificacion_bxp")
    @Temporal(TemporalType.DATE)
    private Date fechaModificacionBxp;
    @Basic(optional = false)
    @Column(name = "usu_modificacion_bxp")
    private int usuModificacionBxp;
    @JoinColumn(name = "id_bodega_bxp", referencedColumnName = "id_bodega")
    @ManyToOne(optional = false)
    private Bodega idBodegaBxp;
    @JoinColumn(name = "id_producto_bxp", referencedColumnName = "id_producto")
    @ManyToOne(optional = false)
    private Producto idProductoBxp;

    public BodegaExistencia() {
    }

    public BodegaExistencia(Integer idbodegaExistencia) {
        this.idbodegaExistencia = idbodegaExistencia;
    }

    public BodegaExistencia(Integer idbodegaExistencia, Date fechaCreacionBxp, int usuarioCreacionBxp, Date fechaModificacionBxp, int usuModificacionBxp) {
        this.idbodegaExistencia = idbodegaExistencia;
        this.fechaCreacionBxp = fechaCreacionBxp;
        this.usuarioCreacionBxp = usuarioCreacionBxp;
        this.fechaModificacionBxp = fechaModificacionBxp;
        this.usuModificacionBxp = usuModificacionBxp;
    }

    public Integer getIdbodegaExistencia() {
        return idbodegaExistencia;
    }

    public void setIdbodegaExistencia(Integer idbodegaExistencia) {
        this.idbodegaExistencia = idbodegaExistencia;
    }

    public Short getCantidadBxp() {
        return cantidadBxp;
    }

    public void setCantidadBxp(Short cantidadBxp) {
        this.cantidadBxp = cantidadBxp;
    }

    public Date getFechaCreacionBxp() {
        return fechaCreacionBxp;
    }

    public void setFechaCreacionBxp(Date fechaCreacionBxp) {
        this.fechaCreacionBxp = fechaCreacionBxp;
    }

    public int getUsuarioCreacionBxp() {
        return usuarioCreacionBxp;
    }

    public void setUsuarioCreacionBxp(int usuarioCreacionBxp) {
        this.usuarioCreacionBxp = usuarioCreacionBxp;
    }

    public Date getFechaModificacionBxp() {
        return fechaModificacionBxp;
    }

    public void setFechaModificacionBxp(Date fechaModificacionBxp) {
        this.fechaModificacionBxp = fechaModificacionBxp;
    }

    public int getUsuModificacionBxp() {
        return usuModificacionBxp;
    }

    public void setUsuModificacionBxp(int usuModificacionBxp) {
        this.usuModificacionBxp = usuModificacionBxp;
    }

    public Bodega getIdBodegaBxp() {
        return idBodegaBxp;
    }

    public void setIdBodegaBxp(Bodega idBodegaBxp) {
        this.idBodegaBxp = idBodegaBxp;
    }

    public Producto getIdProductoBxp() {
        return idProductoBxp;
    }

    public void setIdProductoBxp(Producto idProductoBxp) {
        this.idProductoBxp = idProductoBxp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idbodegaExistencia != null ? idbodegaExistencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BodegaExistencia)) {
            return false;
        }
        BodegaExistencia other = (BodegaExistencia) object;
        if ((this.idbodegaExistencia == null && other.idbodegaExistencia != null) || (this.idbodegaExistencia != null && !this.idbodegaExistencia.equals(other.idbodegaExistencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.srn.jpa.entity.BodegaExistencia[ idbodegaExistencia=" + idbodegaExistencia + " ]";
    }
    
}

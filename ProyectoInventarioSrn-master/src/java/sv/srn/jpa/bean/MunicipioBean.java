/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.bean;
import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import sv.srn.jpa.controller.MunicipioJpaController;
import sv.srn.jpa.entity.Municipio;
/**
 *
 * @author angel
 */
@ManagedBean
@SessionScoped
public class MunicipioBean {
    private EntityManagerFactory factory;
    private MunicipioJpaController controller;
    private Municipio actual = null;

    /**
     * Creates a new instance of MunicipioBean
     */
    public MunicipioBean() {
        factory = Persistence.createEntityManagerFactory("ProyectoInventarioSrnPU");
        controller = new MunicipioJpaController(factory);
    } 
       public Collection<Municipio> getItems() {
       return controller.findMunicipioEntities();
    }
       public Municipio getActual() {
       if (actual == null) {
       actual = new Municipio();
 }
 return actual;
 }
    public String crearMunicipio() throws Exception{
    Municipio MinicipioN = new Municipio();
   actual.setNombreMun(actual.getNombreMun());
   actual.setIdMunicipio(actual.getIdMunicipio());
   controller.create(actual);
   actual=null;
   return "/municipio/listado";
   
   }
    
}

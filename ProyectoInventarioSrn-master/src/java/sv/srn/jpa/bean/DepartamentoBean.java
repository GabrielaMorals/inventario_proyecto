/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.bean;
import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import sv.srn.jpa.controller.DepartamentoJpaController;
import sv.srn.jpa.entity.Departamento;
/**
 *
 * @author angel
 */
@ManagedBean
@SessionScoped
public class DepartamentoBean {
private EntityManagerFactory factory;
private DepartamentoJpaController controller;
private Departamento actual = null;
    /**
     * Creates a new instance of DepartamentoBean
     */
    public DepartamentoBean() {
        factory = Persistence.createEntityManagerFactory("ProyectoInventarioSrnPU");
        controller = new DepartamentoJpaController(factory);

    }
    
    public Collection<Departamento>getItems(){
    return controller.findDepartamentoEntities();
    }
    
    public Departamento getActual(){
        if(actual == null){
            actual = new Departamento();
        }
        return actual;
    }
        
   public String crearDepartamento() throws Exception{
   Departamento Nombredep = new Departamento();
   actual.setNombreDepartamento(actual.getNombreDepartamento());
   actual.setIdPais(actual.getIdPais());
   actual.setIdDepartamento(actual.getIdDepartamento());
   controller.create(actual);
   actual=null;
   return "/departamento/listado";
  
   }
    }

    

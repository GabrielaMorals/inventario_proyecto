/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.bean;

import java.util.Collection;
import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import sv.srn.jpa.controller.ProveedorJpaController;
import sv.srn.jpa.controller.exceptions.IllegalOrphanException;
import sv.srn.jpa.controller.exceptions.NonexistentEntityException;
import sv.srn.jpa.entity.Proveedor;
/**
 *
 * @author angel
 */
@ManagedBean
@SessionScoped
public class ProveedorBean {
private EntityManagerFactory factory;
private ProveedorJpaController controller;
private Proveedor actual=null;

    /**
     * Creates a new instance of ProveedorBean
     */
    public ProveedorBean() {
 factory = Persistence.createEntityManagerFactory("ProyectoInventarioSrnPU");
 controller = new ProveedorJpaController(factory);
    }
    public Collection<Proveedor>getItems() {
   return controller.findProveedorEntities();   
}
   public Proveedor getActual(){
        if (actual == null) {
            actual = new Proveedor();
}
        return actual;
   
       }
         public String crearProveedor(){
        Date fechaDeHoy = new Date();
        actual.setFechaCreacionPrv(fechaDeHoy);
        actual.setFechaModificacionPrv(fechaDeHoy);
        controller.create(actual);
        actual=null;
        return "/proveedor/listado";    
}
            
    public String modificarProveedor() {
        try {
            actual.setFechaCreacionPrv(new Date());
            controller.edit(actual);
            actual = null;
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null,
            new FacesMessage("Se produjo un error: " + ex.getMessage()));
            return null;
        }
        return "/proveedor/listado";
}
 public String prepararEdicion(Proveedor proveedor) {
 actual = proveedor;
 return "/proveedor/editar";
 }
 public String eliminar(Integer idProveedor) {
        try {
             controller.destroy(idProveedor);
        } catch (IllegalOrphanException | NonexistentEntityException ex) {
             FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Se produjo un error: ", ex.getMessage()));
        return null;
        }
        FacesContext.getCurrentInstance().addMessage(null,
             new FacesMessage("Registro borrado"));
        return "/proveedor/listado";
        }
}

    


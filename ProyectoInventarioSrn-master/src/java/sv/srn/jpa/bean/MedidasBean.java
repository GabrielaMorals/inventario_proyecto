/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import sv.srn.jpa.controller.MedidasJpaController;
/**
 *
 * @author angel
 */
@ManagedBean
@SessionScoped
public class MedidasBean {
private EntityManagerFactory factory;
 private MedidasJpaController controller;
    /**
     * Creates a new instance of MedidasBean
     */
    public MedidasBean() {
        factory = Persistence.createEntityManagerFactory("AlmacenPU");
        controller = new MedidasJpaController(factory);

    }
    
}

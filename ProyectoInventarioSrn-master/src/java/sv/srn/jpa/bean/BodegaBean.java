/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.bean;
import java.util.Collection;
import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import sv.srn.jpa.controller.BodegaJpaController;
import sv.srn.jpa.controller.exceptions.IllegalOrphanException;
import sv.srn.jpa.controller.exceptions.NonexistentEntityException;
import sv.srn.jpa.entity.Bodega;

/**
 *
 * @author angel
 */
@ManagedBean
@SessionScoped
public class BodegaBean {
    private EntityManagerFactory factory;
    private BodegaJpaController controller;
    private Bodega actual = null;
    /**
     * Creates a new instance of BodegaBean
     */
    public BodegaBean() {
          factory = Persistence.createEntityManagerFactory("ProyectoInventarioSrnPU");
          controller = new BodegaJpaController(factory);
    }
    public Collection<Bodega> getItems() {
    return controller.findBodegaEntities();
 } 
    
 public Bodega getActual() {
 if (actual == null) {
 actual = new Bodega();
 }
 return actual;
 }

 public String crearBodega() {
 Date fechaDeHoy = new Date();
 actual.setFechaCreacionBod(fechaDeHoy);
 actual.setFechaModificacionBod(fechaDeHoy);
 controller.create(actual);
 actual = null;
 return "/bodega/listado";
 }
 
 public String modificarBodega() {
 try {
 actual.setFechaModificacionBod(new Date());
 controller.edit(actual);
 actual = null;
 } catch (Exception ex) {
 FacesContext.getCurrentInstance().addMessage(null,
 new FacesMessage("Se produjo un error: " + ex.getMessage()));
 return null;
 }
 return "/bodega/listado";
 }   
 
 public String prepararEdicion(Bodega bodega) {
 actual = bodega;
 return "/bodega/editar";
 }
 public String eliminar(Integer idBodega) {
  try {
 controller.destroy(idBodega);
 } catch (IllegalOrphanException | NonexistentEntityException ex) {
 FacesContext.getCurrentInstance().addMessage(null,
 new FacesMessage(FacesMessage.SEVERITY_ERROR,
 "Se produjo un error: ", ex.getMessage()));
 return null;
 }
 FacesContext.getCurrentInstance().addMessage(null,
 new FacesMessage("Registro borrado"));
 return "/bodega/listado";
 }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.bean;


import java.util.Collection;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import sv.srn.jpa.controller.PaisJpaController;
import sv.srn.jpa.controller.exceptions.IllegalOrphanException;
import sv.srn.jpa.controller.exceptions.NonexistentEntityException;
import sv.srn.jpa.entity.Pais;



/**
 *
 * @author angel
 */
@ManagedBean
@SessionScoped
public class PaisBean {
   private EntityManagerFactory factory;
   private PaisJpaController controller;
   private Pais actual = null;
   
    /**
     * Creates a new instance of PaisBean
     */
    public PaisBean() {
        factory = Persistence.createEntityManagerFactory("ProyectoInventarioSrnPU");
        controller = new PaisJpaController(factory);
      }
    public Collection<Pais>getItems(){
    return controller.findPaisEntities();
    }
    
   public Pais getActual(){
   if(actual == null){
   actual= new Pais();}
   return actual;
   }
   
   public String crearPais() throws Exception{
   Pais paisadd = new Pais();
   actual.setIdPais(actual.getIdPais());
   actual.setNombrePais(actual.getNombrePais());
   controller.create(actual);
   actual=null;
   return "/pais/listado";
   
   }
   
   public String modificarPais() throws Exception{
   actual.setIdPais(actual.getIdPais());
   actual.setNombrePais(actual.getNombrePais());
   controller.edit(actual);
   actual=null;
   
   return "/pais/lisdado";
   }
   
public String prepararEdicion(Pais pais) {
 actual = pais;
 return "/pais/editar";
 }


}

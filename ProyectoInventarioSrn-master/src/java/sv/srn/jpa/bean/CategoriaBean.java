/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.srn.jpa.bean;
import java.util.Collection;
import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import sv.srn.jpa.controller.CategoriaJpaController;
import sv.srn.jpa.controller.exceptions.IllegalOrphanException;
import sv.srn.jpa.controller.exceptions.NonexistentEntityException;
import sv.srn.jpa.entity.Categoria;

/**
 *
 * @author angel
 */
@ManagedBean
@SessionScoped
public class CategoriaBean {
    private EntityManagerFactory factory;
    private CategoriaJpaController controller;
    private Categoria actual=null;

    /**
     * Creates a new instance of CategoriaBean
     */
    public CategoriaBean() {
        factory = Persistence.createEntityManagerFactory("ProyectoInventarioSrnPU");
        controller = new CategoriaJpaController(factory);
    }
    
    public Collection<Categoria> getItems() {
    return controller.findCategoriaEntities();
 } 
    public Categoria getActual(){
        if (actual == null) {
            actual = new Categoria();
        }
        return actual;
    }
    public String crearCategoria(){
        Date fechaDeHoy = new Date();
        actual.setFechaCreacionCat(fechaDeHoy);
        actual.setFechaModificacionCat(fechaDeHoy);
        controller.create(actual);
        actual=null;
        return "/categoria/listado";
    }
    public String modificarCategoria() {
        try {
            actual.setFechaCreacionCat(new Date());
            controller.edit(actual);
            actual = null;
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null,
            new FacesMessage("Se produjo un error: " + ex.getMessage()));
            return null;
        }
        return "/categoria/listado";
    }
    public String prepararEdicion(Categoria categoria) {
        actual = categoria;
        return "/categoria/editar";
    }
    public String eliminar(Integer idCategoria) {
        try {
             controller.destroy(idCategoria);
        } catch (IllegalOrphanException | NonexistentEntityException ex) {
             FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Se produjo un error: ", ex.getMessage()));
        return null;
        }
        FacesContext.getCurrentInstance().addMessage(null,
             new FacesMessage("Registro borrado"));
        return "/categoria/listado";
        }
}
